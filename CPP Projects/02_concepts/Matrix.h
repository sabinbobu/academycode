#pragma once

#include <initializer_list>
#include <iostream>
#include <stdexcept>

template < typename T >
class matrix{
public: 
    matrix(const std::initializer_list<std::initializer_list<T> > &l) :
        lines{l.size()}
    {
        buf = new T*[lines];
        int i{};
        for (auto line : l) {
            cols = line.size();
            buf[i] = new T[cols];
            int j{};
            for (auto e : line)
                buf[i][j++] = e; 
            i++;
        }
    }

    // copy construct
    matrix(const matrix &other) : lines{other.lines}, cols{other.cols}
    {
        buf = new T*[lines];
        for(int i = 0; i < lines; i++)
        {
            buf[i] = new T[cols];
            for(int j = 0; j < cols; j++)
            {
                buf[i][j] = other.buf[i][j];
            }
        }
    }
    
    ~matrix()
    {
        for (int i = 0; i < lines; i++)
            delete[] buf[i];
        delete[] buf;
    }
    
    friend std::ostream &operator<<(std::ostream &out, const matrix &m)
    {
        for(int i = 0; i < m.lines; i++){
            for(int j = 0; j < m.cols; j++)
                out << m.buf[i][j] <<" ";
            out<<"\n";
        }
        
        return out;
    }

    matrix &operator=(const matrix &other)
    {
        for (int i=0; i<lines; i++)
        {
            delete [] buf[i];
        }
        delete [] buf;

        lines = other.lines;
        cols = other.cols;


        buf = new T*[lines];
        for(int i=0; i<lines; i++)
        {
            buf[i] = new T[cols];
            for(int j=0; j<cols; j++)
            {
                buf[i][j] = other.buf[i][j];
            }
        }
        return *this;
    }



    friend matrix operator+(const matrix &m1, T t)
    {
        matrix result{m1};
        for (int i=0; i<result.lines; i++)
        {
            for (int j=0; j<result.cols; j++)
            {
                result.buf[i][j] += t;
            }
        }
        return result;
    }

    friend matrix operator*(const matrix &m1, unsigned t)
    {
        matrix result{m1};
        for (int i=0; i<result.lines; i++)
        {
            for (int j=0; j<result.cols; j++)
            {
                result.buf[i][j] = result.buf[i][j] * t;
            }
        }
        return result;
    }


    friend matrix operator+(const matrix &m1, const matrix &m2)
    {
        if(m1.lines != m2.lines || m1.cols != m2.cols)
            throw std::invalid_argument{"Lines/Cols missmatched."};

        matrix result{m1};
        
        for( int i=0; i<result.lines; i++)
        {
            for(int j=0; j<result.cols; j++)
            {
                result.buf[i][j] += m2.buf[i][j];
            }
        }
        return result;
    }

    friend matrix operator-(const matrix &m1, const matrix &m2)
    {
        if(m1.lines != m2.lines || m1.cols != m2.cols)
            throw std::invalid_argument{"Lines/Cols missmatched."};

        matrix result{m1};
        
        for( int i=0; i<result.lines; i++)
        {
            for(int j=0; j<result.cols; j++)
            {
                result.buf[i][j] -= m2.buf[i][j];
            }
        }
        return result;
    }

    friend matrix operator-(const matrix &m1, T t)
    {
        return m1 + (-t);
    }



private:
    T **buf{};
    long unsigned int lines{};
    long unsigned int cols{};
    
};
