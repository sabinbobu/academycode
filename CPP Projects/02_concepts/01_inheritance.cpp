#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Shape2D // clasa abstracta pentru ca nu are implementarea completa ( are functii virtuale ) 
{
    protected:
        string m_name{};
        int m_x{};
        int m_y{};
    public:
        Shape2D(string name, int x, int y) : 
            m_name{name},
            m_x{x},
            m_y{y} { cout << "Shape()\n"; }

        virtual ~Shape2D() {} // destructor  -  metoda virtuala 
        virtual double area() const = 0; // functie pur virtuala ( abstracta )
        virtual double perimeter() const = 0; // functie pur virtuala ( abstracta )

        virtual int getX() const { return m_x;}
        virtual int getX()  { return m_x;} // pentru a putea face override pe o f numai faca este virtuala
        virtual int getY() const { return m_y; }

        friend ostream &operator<<( ostream &out, const Shape2D &s)
        {
            out << s.m_name << ": (" << s.m_x << ", " << s.m_y <<")";
            return out;
        }
};


class Rectangle : public Shape2D // mosteneste clasa Shape2D
{
    protected: 
        unsigned width{};
        unsigned height{};
    
    public:
        Rectangle(int x, int y, unsigned w, unsigned h):
            Shape2D{"rectangle", x, y}, // apel super constructor
            width{w},
            height{h} {}
        
        // ctor overloadind 
        Rectangle(unsigned w, unsigned h):
            Rectangle(0, 0 , w, h) // delegating constructor... astfel trebuie sa fie singura lista de egalizare
            {}
        
        virtual double area() const override { return width * height; }
        virtual double perimeter() const override { return 2 * (width * height); }
        // int getX() const override { cout << "Special x:"; return x; }

};

int main()
{
    // Shape2D s{"rectangle", 10, 10}; // nu-l putem construi pentru ca avem functii abstracte in clasa ( area() si perimeter() )
    // s.area();
    
    Rectangle r{10, 10, 30, 30};
    Shape2D &s_cpp{ r }; // C++
    Shape2D *s_c{ &r }; // C

    cout << r.getX() << endl;
    cout << r.area() << endl;
    cout << "r{10, 10, 30, 30}: " << s_cpp.area() << endl;
    cout << "r{10, 10, 30, 30} " << s_cpp.getX() << endl;

    cout << r << endl;

    cout << s_cpp.getX() << endl;

    vector<Shape2D*> shapes{
        new Rectangle{1, 2, 3, 4},
        new Rectangle{5, 6, 7, 8},
        new Rectangle{2, 4, 6, 8}
    };

    // Polimorfism:
    for (auto shape: shapes)
    {
        cout << "Area: " << shape->area() << endl;
        cout << "Perimeter: " << shape->perimeter() << endl;
        cout << "x: " << shape-> getX() << endl;
    }

    // Apel polimorfic 
    // 1. lucrez cu pointeri sau cu referinte
    // 2, functia apelata este virtuala

    for (auto s : shapes)
        delete s;

    return 0;
}