
#include <iostream>
#include <vector>
#include "Matrix.h"

using namespace std;

string operator*(string s, unsigned n)
{
    string res{};
    for (unsigned i = 0; i < n; i++)
        res += s;
    return res;
}

template<typename T>
vector<T> operator*(vector<T> v, unsigned n)
{
    vector<T> res{};
    for (unsigned i = 0; i < n; i++)
        res.insert(res.end(), v.begin(), v.end());
    return res;
}

int main()
{
    try {
        matrix<int> m1{{1, 2, 3}, {4, 5, 6}};
        matrix<int> m2{{3, 4, 5}, {6, 7, 8}};
        auto m3{m1 + m2};
        auto m4{m2 + 1};
        
        m4 = (m4 - 1) + 2;

        cout << m1 << endl;

        cout << m4 << endl;

        cout << m3 << endl;

        auto m5{((m1 + m2) + 10 - 5) * 10};
        cout << m5 << endl;

        string s1 {"cucu"};
        s1 += "bau";
        cout << s1 << endl;
        

        matrix<string> m6{{"Ana", "Maria"}, {"Stefan", "Marius"}};
        auto m7{m6 + "_SUFFIX"};
        m7 = m7 * 2;
        
        cout << m7 << endl;

        vector<int> numere{1, 2, 3};
        auto v2 {numere * 100};
        for (auto e: v2)
            cout << e << " ";
        cout << endl;

    } catch(invalid_argument &e) {
        cerr << e.what() << endl;
    } catch(...) {
        cerr << "Other exception\n";
    }
   
}
