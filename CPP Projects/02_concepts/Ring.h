#pragma once
#include <iostream>
#include <initializer_list>

template <typename T>
class ring
{       
    public:
        ring(unsigned capacity, std::initializer_list<T> l): 
            capacity{capacity}
            {
                buff = new T[capacity];
                for(T t : l)
                {
                    add(t);
                }
            }
        
        ~ring() {
            delete [] buff;
        }
        
        void add(T t)
        {
            if(size==capacity)
            {
                overflow = true;
                size = 0;
            }
            buff[size++] = t;
        }
        
        T get(unsigned index)
        {
            return buff[index%capacity];
        }
        
        unsigned get_size() const { return overflow ? capacity:size;}
        
        ring(const ring &a) : 
            capacity{a.capacity},
            size{a.size},
            overflow{a.overflow}
            {
                buff = new T[capacity];
                for(unsigned i = 0; i<capacity; i++)
                {
                    buff[i] = a.buff[i];
                }   
            }

        friend std::ostream &operator<<(std::ostream &out, const ring &a)
        {
            for (unsigned i = 0; i < a.get_size(); i++)
            {
                out<<a.buff[i]<<" ";
            } 
            return out;
        }
        
        class iterator;
        
        iterator begin()
        {
            return iterator{0, *this};
        }
        
        iterator end()
        {
            return iterator(get_size(), *this);
        }
        
    private:
        unsigned size{};
        unsigned capacity{};
        T *buff{};
        bool overflow{};
};


template <typename T>
class ring<T>::iterator
{
public:
    iterator(int pos, ring &r) :
        pos{pos},
        m_ring{r}
    {}
    
    T operator*() { return m_ring.get(pos); }
    
    bool operator!=(const iterator &other)
    {
        return this->pos==other.pos ? false : true;
    }
    
    bool operator==(const iterator &other)
    {
        return this->pos==other.pos;
    }
    
    iterator operator++()
    {
        this->pos++;
        return *this;
    }
    
    iterator operator++(int)
    {
        iterator temp{*this};
        this->pos++;
        return temp;
    }
    
private:
    int pos;
    ring &m_ring;  // atentie la copiere
};















