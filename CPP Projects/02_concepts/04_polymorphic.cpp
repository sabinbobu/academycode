#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Shape2D // clasa abstracta pentru ca nu are implementarea completa ( are functii virtuale ) 
{
    protected:
        string m_name{};
        int m_x{};
        int m_y{};
    public:
        Shape2D(string name, int x, int y) : 
            m_name{name},
            m_x{x},
            m_y{y} { cout << "Shape(name, x, y)\n"; }

        Shape2D(const Shape2D &other) : // copy ctor
            m_name{other.m_name}, 
            m_x{other.m_x},
            m_y{other.m_y}
            {
                cout<< "S-a apelat copy constructor..." << endl;
            }

        virtual ~Shape2D() { cout << "~Shape2d()\n"; } // destructor  -  metoda virtuala 
         virtual double area() const { return 0; } // not virtual = > not polymorphic
         virtual double perimeter() const { return 0; } // not virtual = > not polymorphic

        virtual int getX() const { return m_x;}
        virtual int getX()  { return m_x;} // pentru a putea face override pe o f numai faca este virtuala
        virtual int getY() const { return m_y; }

        friend ostream &operator<<( ostream &out, const Shape2D &s)
        {
            out << s.m_name << ": (" << s.m_x << ", " << s.m_y <<")";
            return out;
        }
};


class Rectangle : public Shape2D // mosteneste clasa Shape2D
{
    protected: 
        unsigned width{};
        unsigned height{};
    
    public:
        Rectangle(int x, int y, unsigned w, unsigned h):
            Shape2D{"rectangle", x, y}, // apel super constructor
            width{w},
            height{h} {}
        
        // ctor overloadind 
        Rectangle(unsigned w, unsigned h):
            Rectangle(0, 0 , w, h) // delegating constructor... astfel trebuie sa fie singura lista de egalizare
            {}
        
        virtual ~Rectangle()
            {
                cout << "~Rectangle()\n";
            }
        
        virtual double area() const override { return width * height; }
        virtual double perimeter() const override { return 2 * (width * height); }
        // int getX() const override { cout << "Special x:"; return x; }

        friend ostream &operator << ( ostream &out, const Rectangle &r)
        {
            out << r.m_name << ": (" << r.m_x << ", " << r.m_y << ")  ";
            out << "Area: " << r.area() << ", perimeter: " << r.perimeter();
            return out;
        }

};

void display_notOK(Shape2D s){ // object slicing ( se taie din obiect si se folosesc numai anumite parti din clase )
    cout <<"Area:" <<  s.area() << endl;
    cout << "Perimeter: " << s.perimeter() << endl;
    cout << s << endl;

}

void display_OK_adresa(Shape2D &s){ // object slicing ( se taie din obiect si se folosesc numai anumite parti din clase )
    cout <<"Area:" <<  s.area() << endl;
    cout << "Perimeter: " << s.perimeter() << endl;
    cout << s << endl;

}

void display_OK_pointer(Shape2D *s){ // object slicing ( se taie din obiect si se folosesc numai anumite parti din clase )
    cout <<"Area:" <<  s->area() << endl;
    cout << "Perimeter: " << s->perimeter() << endl;
    cout << *s << endl;

}



int main()
{
    // Shape2D s{"rectangle", 10, 10}; // nu-l putem construi pentru ca avem functii abstracte in clasa ( area() si perimeter() )
    // s.area();
    
    Rectangle r{10, 10, 30, 30};
    Shape2D &s_cpp{ r }; // C++
    Shape2D *s_c{ &r }; // C

    cout << "--------------- display_notOK(Shape2D s) ----------------" <<endl;
    display_notOK(r); 
    cout << "------------- display_OK_adresa(Shape2D &s) --------------------" << endl ;
    display_OK_adresa(s_cpp); // sau arg : r

    cout << "------------- display_OK_pointer(Shape2D *s) --------------------" << endl ;
    display_OK_pointer(s_c);

    cout << " -------------- Suprascriere << : ---------------------"<< endl;
    cout << r << endl;



   

    // Apel polimorfic 
    // 1. lucrez cu pointeri sau cu referinte
    // 2, functia apelata este virtuala

    
    return 0;
}