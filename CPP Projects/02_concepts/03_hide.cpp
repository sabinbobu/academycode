#include <iostream>
#include <string>
#include <mutex>

 
using namespace std;

 
template<typename T>
class Stack
{
public:
    Stack(unsigned capacity) : m_capacity{capacity}
    {
        m_buf = new T[m_capacity];
    }
    
    //delete -> nu vreau sa imi dea un default copy constructor; nu permitem copierea; evitam unele probleme; nu putem face copierea deoarece mutexul are by default copy constructor  delete;
    //Stack(const Stack &other) = delete;
    
    //delete sterge functia, nu o ascunde
    ///Stack &operator=(const Stack &other) = delete;
    
    virtual ~Stack()
    {
        cout << "~stack" << endl;
        delete[] m_buf;
    }

    friend void f();

private:
    T *m_buf{};
    unsigned m_capacity{};
    unsigned m_size{};
    mutex m_mutex{};
    Stack(const Stack &other){}
    Stack &operator=(const Stack &other) {}
};


template<typename T>
void g(Stack<T> s)
{

}


void f()
{
    Stack<int> s1{10};
    g(s1);
}

 
int main()
{
    Stack<int> s1{10};
    Stack<int> *s2 {new Stack<int>{10}};//aici e memory leak; trebe facut delete aici; asta traieste pe heap si deci trebe delete
    
    //auto s3{s1};
    f();
   
    delete s2;
    return 0;
}