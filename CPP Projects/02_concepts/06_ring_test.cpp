#include <iostream>
#include <string>

#include "Ring.h"

using namespace std;


int main()
{
    ring<string>r{3, {"Ana", "Maria"}};
    r.get_size() == 2;
    r.add("Ion");
    r.add("George");
    r.get(0) == "George";
    
   
    auto r2{r};
    cout << r2 << endl;

    for (ring<string>::iterator it = r2.begin(); it != r2.end(); ++it) // old school iteration
    {
        cout << *it << endl;
    }


    
     // r2 = r3; // overriding =
    return 0;

}