#include <iostream>

using namespace std;

class Empty
{
    public:
        Empty() { cout <<  "Empty()\n"; }
        // copy constructor 
        Empty (const Empty &other) { cout << "Copy ctor\n"; }
        Empty &operator = (const Empty &other) { cout << "Assignement op\n"; return *this; }
        virtual ~Empty() { cout << "~Empty()\n"; }
        
        void f(){}
};

int main()
{
    Empty e1{};
    Empty e2{e1};
    
    Empty e3{};
    e2 = e3; 

    cout << sizeof(e2) << endl;
    return 0;
}