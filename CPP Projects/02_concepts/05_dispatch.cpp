#include <iostream>
#include <string>

using namespace std;

class Derived; // forward declaration
class Base;

class Base
{
    protected:
        int x{};
    public:
        Base(int x) :
            x{x} {}

        virtual ~Base() = default;

        virtual void f() const { cout << "Base::f()\n"; 
        virtual void f(const Derived &d) const { cout << "Base::f(d)\n"; }
        virtual void f(const Base &b) const { cout << "Base::f(b)\n"; }
};

class Derived : public Base
{
    public:
        Derived(int x) :
            Base(x) {}

        virtual ~Derived() = default;

        virtual void f() const override { cout << "Derived::f()\n"; }
        virtual void f(const Derived &d) const override { cout << "Derived::f(d)\n"; }
        virtual void f(const Base &b) const override { cout << "Derived::f(b)\n"; }
};

void g(Base &b)
{
    cout << "g(b) - param de tip Base" << endl;

}

void g(Derived &d){
    cout << "g(d) - param de tip Derived" << endl;
}

void call_f(const Base &b){
    b.f();
    //  !!! single despatch - >> se uita la obiectul pe care apelez si astfel vom accesa functia din clasa respectiva
    // nu conteaza tipul argumentelor, ci tipul obiectului pe care apelez
    b.f(b); 
}


int main()
{
    Derived d{10};
    call_f(d);
    g(d);

    Base &b(d);
    Base bb{8};
    g(b); // functia potrivita va fi apelata in funtie de tipul param 
    call_f(bb);
    return 0;
}