#include <iostream>
#include <string>

using namespace std;

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}
int main()
{
    int x{1}, y{2};
    cout << "x = " << x << " y = " << y;
    swap(x, y);
    cout << "x = " << x << " y = " << y;

    
    return 0;
}