#pragma once
#include <random>

// using namespace std;  // NEVER!!

namespace edocti { namespace utils
{

class RandomUtil
{
public:
    /** Init the seed => make numbers random */
    static void randomize();

    static int nextInt(int start, int end);

    static double nextDouble(double start, double end);

private:
    static std::default_random_engine &global_engine();
};


} }
