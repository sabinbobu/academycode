#include <iostream>
#include <string> 
#include <typeinfo>

using namespace std;

int main()
{   
    string ana{"Ana"};
    decltype(ana) maria{"Maria"};

    cout << maria << endl;
    cout << typeid(maria).name() << endl;
    cout << typeid(ana).name() << endl;
    return 0;
}