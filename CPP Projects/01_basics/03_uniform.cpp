#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;

void acolade_cpp98(){
    // caz 1
    int a = {1};

    // caz 2
    class person{ // caz 3. struct 
    public:
        string name;
        int age;
    }ion = {"Ion", 25};

    person vasile = {"Vasile", 29};
    cout << vasile.name << endl;

    // caz 4  - struct anonim
    struct {
        string name;
        int age;

    } ionica = {"Ion", 26}, vasilica = {"Vasile", 30};
}

void non_uniform_cpp11(){
    int unu(1);
    int doi = 2;
    int trei{3}; // CPP+ // uniform initialisation 
    int patru = {4}; // CPP+
}

void test_uniform(){
    class A
    {
        public:
            A() {} // initializare default
           // A(int x) : m_x(x), m_y(0), buf(NULL) {} // in constructor, in CPP vechi trebuiau init toate variabilele
           A(int x) : m_x{x} {} // in CPP nou
        private:
            int m_x{}; // acolada face init la default value pentru int
            int m_y{};
            int *buf{}; // NULL - nullptr
    };

    int i{}; // in CPP nou, acoladele fac initializare la default value
    bool b{};

    A a1;
    A a2{};
    A a3{10};

    // Narrowing Conversion Prevention
    // int i1 = 9999999999988899; //  warning: overflow in implicit constant conversion [-Woverflow]
    // cout << i1 << endl;

    long int i1_long = 9999999999988889; 
    cout << i1_long << endl;

    short s{1024};
    cout << s << endl;
    s += i1_long;
    cout << s << endl;

    // int i2{10.0f}; // error: narrowing conversion of ‘1.0e+1f’ from ‘float’ to ‘int’ inside { } [-Wnarrowing]
    int i3 = 10.0f;
    cout << i3 << endl;

    int *pi1{&i3};
    cout << pi1 << endl;

    int numers1[]{1, 2, 3, 4, 5};
    int *pnumbers{ new int[5]{1, 2, 3, 4, 5}}; // alocare memorie pe heap pentru 5 int uri  + init
    for (int i{0}; i < 5; i++){
        cout << pnumbers[i] << " ";
    }
    cout << endl;
    delete[] pnumbers; // dealocare

    int numbers2[5]{2, 3};
    int numbers3[5]{}; // initializare valori cu int 0
    for (int n : numbers2)
        cout << n << " ";
    
    int mat1[2][3]{{1, 2, 3}, {4, 5, 6}};
    int mat2[2][3] {1, 2, 3, 4, 5}; // inline init - compilatorul nu va da eroare - va considera linia 0 : 1 2 3, linia 1: 4 5 
    cout << endl << "Afisare matrice: " << endl;
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            cout << mat2[i][j] << " ";
        }

        cout << endl;
        
    }
    cout << endl;

    vector<int> v1{1, 2, 3, 4, 5};
    for (int e : v1)
        cout << e << " ";
    cout << endl;
    
    vector<int> v2(10, 8);
    for (int e : v2)
        cout << e << " ";
    cout << endl;

    map<string, int> ages{{"ana", 10}, {"maria", 20}};
    cout << "Ana are " << ages["ana"] << " ani." << endl; 

}

int main(){
    cout << "Hello world!\n";
    cout << endl;
    test_uniform();
    return 0;
}
