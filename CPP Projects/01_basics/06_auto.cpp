#include <iostream>
#include <string>
#include <vector>
#include <typeinfo>

using namespace std;
/*
auto - cu auto las compilatorul sa deduca el tipul variabilei
     - util cand nu mai dorim sa precizam tipul functiilor sau a variabilelor

auto f{10.5f};
auto name{"Ana"};


*/

extern int i;  // variabila externa 
void cpp98()
{
   /* auto */  int i = 10; // durata de viata a variabilei este automat durata de viata a stack frame ului 
   // extern int i = 10; // accesare valoare variabila globala
   vector<int> numere;
    numere.push_back(2);
    numere.push_back(3);
    numere.push_back(4);
    // am creat un obiect iterator care stie sa intereze printre ELEMENTE
    for (vector<int>::iterator it = numere.begin(); it != numere.end(); ++it)
    {
        cout << *it << " ";

    }
    cout << endl;
}

void cpp11()
{
    auto i{10}; // cu auto las compilatorul sa deduca el tipul variabilei 
    vector<int> numere{1, 2, 3, 4, 5};
    for(auto it{numere.begin()}; it != numere.end(); ++it)
        cout << *it << " ";
    cout<< endl;

    for (auto &n : numere) // n primeste o referinta a elementelor din numere
    {
        ++n;
    }

    for (auto n : numere) // face o copie a elem din numere
    {
        cout << n << " ";
    }
    cout << endl;

    auto f{10.5f};
    auto name{"Ana"};
    string name2{"Ioana"};
    auto name3{"Maria"s}; // s este un literal pentru tipul string

    cout << typeid(f).name() << endl;
    cout << typeid(name3).name() << endl;
    cout << typeid(name2).name() << endl;

    // auto numbers[]{1, 2, 3, 4};
    int numbers[]{1, 2, 3, 4};
    auto *pnumbers{numbers};
    int *pnumbers2{numbers};

    cout << typeid(pnumbers).name() << " - pointer int" << endl;
    cout << typeid(pnumbers).name() << " - pointer int" << endl;

    for (auto i{0u}; i < 3; i++)
        cout << numbers[i] << " ";
    cout << endl;


}

auto add(int a, int b) { return a + b;}


auto divide_auto(int a, int b) -> double // am ajutat compilatorul si astfel va considera tipul returnat de functie va fi double
{
    if(b == 0)
        return -1;
    return a / b;
}
// ---------------------------------------------------------------------------
class A
{
    private: 
        int x{};
    public:
        A(int x): x{x}{};
        // copy constructor ( ii dam pa param un alt obiect de tipul clasei)
        A(const A &other) : x{other.x} { cout << "A(A&)\n";}

        auto getX() const { return this->x;} // const delcarat astfel, nu de da voia sa modifcam in obiectul respectiv


};

void test_auto_on_class(){
    A a{10};
    auto b{a}; // realizam o copia a lui in in tipul_clasei b


    // REFERINTE CATRE a
    A &c{a};
    auto &d{a};

    auto alt_x { a.getX() };
    cout << " auto alt_x: " << alt_x << endl;

}
// -------------------------------------------------------------------------------

// --------------------- Templates -------------------------------------------
template<typename T, typename U>
auto add(T t, U u) -> decltype(t + u) // trailing return syntax
{
    return t + u;
}

template<typename T, typename U>
auto divide_template(T t, U u) -> decltype(t / u)
{
    if (u == 0 )
        return -1; // decltype(t / u)
    return t / u;
}
int main()
{
    cpp98();
    cpp11();
    test_auto_on_class();
    
    cout << divide_auto(5, 2) << endl;
    cout << divide_template(10.2222, 5.2222) << endl;

    return 0;
}

