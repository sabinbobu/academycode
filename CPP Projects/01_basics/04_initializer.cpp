#include <iostream>
#include <vector>

using namespace std;

// dorim sa avem un nr variabil de argumente pentru constructorul clasei
template<typename T>
class MyCollection
{
    public:
    MyCollection(initializer_list<T> l) : v{l} // initializare cu initializer list // constructor 1
    {
       // for (int e : l){ // initializare old style
       //     v.push_back(e); // fiecare element din lista va fi adaugat in vectorul v
       // }
    }
    MyCollection(const vector<T> &v) : v{v} {} // constructor 2

    // suprascriere ( overloading ) operator 
    // friend - permite accesarea din exterior a functiei prietene cu clasa
    // functia friend ostream << nu este o functie a clasei, dar fiind prietena are acces la membrii privati ai clasei
    // declarare in clasa si implementare in afara clasei
    template<typename U>
    friend ostream &operator<<(ostream &out, const MyCollection<U> &c); // return catre out intreaga referinta ( e, " ", ...)
    
    private:
        vector<T> v{};
};

template<typename T>
ostream &operator<<(ostream &out, const MyCollection<T> &c) // return catre out intreaga referinta ( e, " ", ...)
    {
        for (T e : c.v)
            out << e << " ";
        return out;
    }

template<typename T>
class A
{
public:
    A(T x) : x{x} {}

    template<typename U>
    friend ostream &operator<<(ostream &out, const A<U> &a);
private:
    T x{};
};

template<typename U>
ostream &operator<<(ostream &out, const A<U>&a)
{
    out << a.x;
    return out;
}

int main(){
    MyCollection<int>  c1{1, 2, 3, 4, 5}; // se aplica construcor 1
    cout << "Afisare vector v ( care este un membru privat al clasei ) prin suprascrierea operatorului '<<' folosind o functie de tip friend: " << endl << endl;
    cout << "MyCollection<int>: " << c1 << endl;

    MyCollection<string> names{"Ana", "Maria", "Ioana"};
    cout << "Collection<string> : " << names << endl;

    //MyCollection<A> c2{A{10}, A{20}, A{30}};
    // suprascriem << pentru a a fisa un A
    //cout << "MyCollection<A>: " << c2 << endl;

    MyCollection< A<int> > c2{ A<int>{10}, A<int>{20}, A<int>{30} };
    cout << "MyCollection< A<int> >: " << c2 << endl;

    MyCollection <A<string>> c3{ A<string>{"Sabin"}, A<string>{"Bobu"}};
    cout << "MyCollection < A<string> >: " << c3 << endl;

    //vector<int> v{8, 8, 8}; // se aplica constructor 2
    //MyCollection c1{v};

    //cout << c1 << endl;
    return 0;
}