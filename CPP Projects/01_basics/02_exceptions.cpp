#include <iostream>
#include "RandomUtil.h"

using namespace std;
using namespace edocti::utils;


class A
{
public:
    A() : m_x{0}, m_y{0}
    {
        cout << "A()\n";
    }

    A(int x, int y) : m_x{x}, m_y{y}
    {
        cout << "A(x, y)\n";
    }

    ~A() { cout << "~A()\n"; }

    void display() const { cout << "A::display()\n"; }

private:
    int m_x;
    int m_y;
};


void throw_an_exception()
{
    int err = RandomUtil::nextInt(1, 10);
    cout << err << endl;
    if (err <= 3) {
        throw 100;
    } else if (3 < err && err < 7) {
        throw "const char* exception";
    } else {
        throw string("string exception");
    }
}


void work()
{
    // A a;  // always initialized!!
    A a{10, 20};  // local variable (stack frame)
    A *pa = new A{20, 30};  // sizeof (pa) = 8B (64bit CPU), 4B (32bit CPU)

    throw_an_exception();  // STACK UNWINDING

    a.display();
    pa->display();
    delete pa;  // nu se apeleaza in caz de exceptie
}


int main()
{
    RandomUtil::randomize();
    for (int i = 0; i < 20; i++) {
        try {
            work();
        } catch(int e) {
            cerr << "Caught int exception: " << e << endl;
        } catch(const char *e) {
            cerr << "Caught const char* exception: " << e << endl;
        } catch(string &e) {
            cerr << "Caught string exception: " << e.c_str() << endl;
        } catch(...) {
            cerr << "Caught other exception\n";
        }
    }
    return 0;
}
