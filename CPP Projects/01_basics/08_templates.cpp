#include <iostream>
#include <string>

using namespace std;

// se creaza o functie generica care poate fi apelata apoi cu tipuri de data diferite

template<typename T>
T add1(T a, T b)
{
    return a + b;
}

template<>
int add1(int a, int b)
{
    cout << "template specialization\n";
    return a + b;
}

int add1(int a, int b){
    cout << "specific\n"; return a + b; 
}


auto add_auto(auto a, auto b){
    return a + b;
}
int main()
{
    cout << add1(10, 20) << endl;
    cout << add_auto(20, 40) << endl;

    cout << add1(string("Ana-"), string("Maria")) << endl;
    cout << add_auto(string("Ana-"), string("Maria")) << endl;

    cout << add1(10.2, 34.5) << endl;
    cout << add1<>(10, 10) << endl;
    cout << add1(string("Ana-"), string("Maria")) << endl;
    


    return 0;
}