#include "RandomUtil.h"

using namespace std;

namespace edocti { namespace utils
{
    default_random_engine &RandomUtil::global_engine()
    {
        static default_random_engine engine{};  // ctor default
        return engine;
    }


    void RandomUtil::randomize()
    {
        static random_device device{};  // Intel CPU based
        global_engine().seed(device());  // call operator on device
    }


    int RandomUtil::nextInt(int start, int end)
    {
        static uniform_int_distribution<int> dist{};
        return dist(global_engine(), uniform_int_distribution<int>::param_type{start, end});
    }


    double RandomUtil::nextDouble(double start, double end)
    {
        static uniform_real_distribution<double> dist{};
        return dist(global_engine(), uniform_real_distribution<double>::param_type{start, end});
    }
}}
