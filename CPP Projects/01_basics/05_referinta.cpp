#include <iostream>
#include <string>

using namespace std;

void swap(int *a, int *b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void swap(int &a, int &b) // a si b de tip referinta catre valorile param
{
    int tmp{a};
    a = b;
    b = tmp;
}
int main()
{
    int x{1}, y{2};
    cout << "x = " << x << " y = " << y << endl;
    swap(x, y);
    
    cout << "x = " << x << " y = " << y << endl;

    swap(&x, &y);
    cout << "x = " << x << " y = " << y << endl;
    
    return 0;
}