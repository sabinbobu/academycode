#include <iostream>
#include <fstream>
#include <string>
#include <vector>

// Cot for commit

using namespace std;

class Faculty
{
public:
    Faculty() {}
    Faculty(string name, string city): name{name}, city{city} {}

    string getName() const { return name; }
    void setName(const string &name) { this->name = name; }

    string getCity() const { return city; }
    void setCity(const string &city) { this->city = city; }

private:
    string name{};
    string city{};
};

class Student
{
public:
    Student(string fn, string ln, int age) :
        first_name{fn}, last_name{ln}, age{age} {}

    Student() {}

    friend istream &operator>>(istream &in, Student &s);
    friend ostream &operator<<(ostream &out, const Student &s);

private:
    string first_name{};
    string last_name{};
    int age{};
    Faculty faculty{};
};


istream &operator>>(istream &in, Faculty &f)
{
    string name, city;
    in >> name >> city;
    f.setName(name);
    f.setCity(city);

    return in;
}


ostream &operator<<(ostream &out, const Faculty &f)
{
    out << f.getName() << " din " << f.getCity();
    return out;
}


istream &operator>>(istream &in, Student &s)
{
    in >> s.first_name >> s.last_name >> s.age;
    in >> s.faculty;
    return in;
}


ostream &operator<<(ostream &out, const Student &s)
{
    out << s.first_name << " " << s.last_name;
    out << ", age: " << s.age;
    out << ", faculty: " << s.faculty;
    return out;
}

int main(int argc, char **argv)
{
    Student s{};
    //cout << "Introdu student: "; cin >> s;
    //cout << "Student introdus: " << s << endl;

    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " cale_fisier\n";
        return 1;
    }

    vector<Student> students;
    ifstream f{argv[1]};
    if (!f.is_open()) {
        cerr << "Cannot open file\n";
        return 2;
    }

    while (f >> s) {
        students.push_back(s);
    }

    for (auto s : students)
        cout << s << endl;

    return 0;
}
