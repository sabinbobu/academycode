#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class Person
{
    public:
        string first_name{};
        string last_name{};
        int age{};

        Person(string fn, string ln, int age) :
            first_name{fn}, last_name{ln}, age{age} {}

        Person() {}

        Person(const Person &p):// copy ctor
            first_name{p.first_name}, last_name{p.last_name}, age{p.age} {}

        ~Person(){} // destructor

    
    

}; // end class Person

class Student : public Person
{
public:
    Student(string fn, string ln, int age, vector<int> marks, char gen) :
        Person(first_name, last_name, age),
        marks{marks},
        gen{gen} {}

    Student() {}

    string get_first_name()
    {
        return first_name;
    }

    vector<int> get_marks(){
        return marks;
    }

    friend istream &operator>>(istream &in, Student &s);
    friend ostream &operator<<(ostream &out, const Student &s);

private:
    vector<int> marks;
    char gen;
}; // end class Student

// --- overloading << >> Student --- 

istream &operator>>(istream &in, Student &s) // read
{   int nrMarks;
    in >> s.first_name >> s.last_name >> s.age;
    in >> s.gen;
    in >> nrMarks;
    int mark;
    for(int i = 0; i< nrMarks; i++)
    {
        in >> mark;
        s.marks.push_back(mark); // addNota(nota);
    }
    return in;
}


ostream &operator<<(ostream &out, const Student &s) // write
{
    out << s.first_name << " " << s.last_name;
    out << ", age: " << s.age;
    out << ", gender: " << s.gen;
    out << ", marks: ";
    for( auto m : s.marks)
         out << m << " ";

    return out;
}


class Retiree : public Person
{
public:
    Retiree(string fn, string ln, int age, int salary, char gen) :
        Person(first_name, last_name, age),
        salary{salary},
        gen{gen} {}

    Retiree() {}

    friend istream &operator>>(istream &in, Retiree &r);
    friend ostream &operator<<(ostream &out, const Retiree &r);

private:
    int salary{};
    char gen;
}; // end class Retiree

// --- overloading << >> Retiree --- 

  istream &operator>>(istream &in, Retiree &r){

    in >> r.first_name >> r.last_name >> r.age;
    in >> r.gen;
    in >> r.salary;
    return in;
 }
 ostream &operator<<(ostream &out, const Retiree &r){
    out << r.first_name << " " << r.last_name;
    out << ", age: " << r.age;
    out << ", gender: " << r.gen;
    out << ", salary: " << r.salary;

    return out;
}


class Worker : public Person
{
public:
    Worker(string fn, string ln, int age, int salary, char gen) :
        Person(first_name, last_name, age),
        salary{salary},
        gen{gen} {}

    Worker() {}

    friend istream &operator>>(istream &in, Worker &w);
    friend ostream &operator<<(ostream &out, const Worker &w);

private:
    int salary{};
    char gen;
}; // end class Worker

// --- overloading << >> Worker --- 

  istream &operator>>(istream &in, Worker &r){

    in >> r.first_name >> r.last_name >> r.age;
    in >> r.gen;
    in >> r.salary;
    return in;
 }
 ostream &operator<<(ostream &out, const Worker &r){
    out << r.first_name << " " << r.last_name;
    out << ", age: " << r.age;
    out << ", gender: " << r.gen;
    out << ", salary: " << r.salary;

    return out;
}

// -------- Operatii posibile ----------------------------

// afisarea tuturor persoanelor de un anumit tip
void display_all(string type, vector<Student*> students, vector<Retiree*> retirees, vector<Worker*> workers)
{
    cout << "Alege tipul ( S / R / W): "; cin >> type;

    if(type == "S")
    {
         for (auto s : students)
        cout << *s << endl;
    }
    else if(type == "R")
    {
        for(auto r :  retirees)
        cout << *r << endl;
    }
    else if(type == "W")
    {
       for(auto w : workers)
        cout << *w << endl;
    }
}


// afisarea celei mai mari note ale studentului X
void max_mark(string first_name, vector<Student*> students)
{
    cout << " Introduceti first name-ul studentului: "; cin >> first_name;
    int max_m{};
    for ( auto s: students)
    {
        if(s->get_first_name() == first_name)
        {
            for( auto m : s->get_marks() )
            {
                if( m > max_m)
                {
                    max_m = m;
                }
            }
        }
    }

    cout << "Nota maxima a studentului este: " << max_m << endl;
}

void averages_display(vector<Student> students)
{
    for( auto s in students)
    {
        
    }
}

int main(int argc, char **argv)
{
    vector<Student*> students; // vector de Student pointeri 
    vector<Retiree*> retirees;
    vector<Worker*> workers;

// File reading
    if (argc != 2) {
        cerr << "Usage: " << argv[0] << " cale_fisier\n";
        return 1;
    }

    ifstream f{argv[1]};
    if (!f.is_open()) {
        cerr << "Cannot open file\n";
        return 2;
    }
// citire si construiere vector<Tip>

string type;
while (f >> type)
{
    if(type == "S")
    {
        Student *s = new  Student{};
        f >> *s;
        students.push_back(s);
    }
    else if(type == "R")
    {
        Retiree *r =  new Retiree{};
        f >> *r;
        retirees.push_back(r);
    }
    else if(type == "W")
    {
        Worker *w = new Worker{};
        f >> *w;
        workers.push_back(w);
    }
}


// Operatii pe date
       
    string first_name;
    int choice;

// ------------------- Meniu ---------------
    
    cout << "Operatii posibile: " << endl;
    cout << "1 - Afisarea tuturor studentilor." << endl;
    cout << "2 - Aflarea notei maxime pentru un student oarecare." << endl;
    cout << "3 - va urma ..." << endl;

    cout << "Alegeti operatia:" << endl;
    cin >> choice;

while(1)
{
    switch (choice)
    {
    case 1:
        display_all(type, students, retirees, workers);
        break;
    case 2:
        max_mark(first_name,students);
        break;
    default: 
        cout << "Optiune indisponibila.";
        break;
    }
}

    return 0;
}
