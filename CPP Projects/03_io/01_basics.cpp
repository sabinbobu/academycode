#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

int main(int argc, char **argv)
{
    int x{}, y{};
    string name{};
    cout << "x= "; cin >> x;
    cout << "y= "; cin >> y;
    cout << x+y << endl;

    cout << "name: "; cin >> name;
    cout << "Numele introdus este: " << name << endl ;

    if (argc !=2)
    {
        cerr << "Usage: " << argv[0] << " cale_fisier\n";
        return 1;
    }
    // creare stream pentru fisier
    ifstream f{argv[1]}; // input file stream
    if(!f.is_open())
    {
        cerr << "Cannot open "<< argv[1] << endl;
        return 2;
    }

    string line{};
    while(getline(f, line))
    {
        cout << line << endl;
    }

    f.close();

    string word{}; // f citeste cuvant cu cuvant
    f = ifstream{argv[1]};
    f.seekg(0, f.beg);

    while(f >> word){
        cout << "Citire string cu string: " <<  word << endl; 
    }
    f.close();

    f = ifstream{argv[1]};
    double d{};
    string oras{};
    getline(f, line);

    f >> x >> y << d << oras; // din file f il citim pe x si y

    cout << "x, y si d = " << x << ", " << y << " si " << d << endl;
    cout << "oras = " << oras << endl;
    return 0;
}