#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>


using namespace std;

class Base
{
public:
    Base(string name, int age) : name{name}, age{age} {}

    virtual string str() const
    {
        stringstream ss{};
        ss << name << ", " << age;
        return ss.str();
    }

    friend ostream &operator<<(ostream &out, const Base &b);

protected:
    string name{};
    int age{};
};


class Derived : public Base
{
public:
    Derived(string name, int age, string color):
        Base{name, age},
        color{color}
    {}

    virtual string str() const override
    {
        stringstream ss{};
        ss << name << ", " << age << ", " << color;
        return ss.str();
    }

    //friend ostream &operator<<(ostream &out, const Derived &d);

private:
    string color{};
};

ostream &operator<<(ostream &out, const Base &b)
{
    out << b.str();
    return out;
}

//ostream &operator<<(ostream &out, const Derived &d)
//{
//    out << d.name << ", " << d.age << ", " << d.color;
//    return out;
//}


int main()
{
    vector<Base*> objects{
        new Derived{"Ana", 20, "alb"},
        new Derived{"Maria", 20, "alb"}
    };

    for (auto o : objects)
        cout << *o << endl;

    for (auto o : objects)
        delete o;

    return 0;
}
