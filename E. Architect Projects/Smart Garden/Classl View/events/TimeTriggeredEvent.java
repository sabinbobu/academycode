package events;


/**
 * @author F86588D
 * @version 1.0
 * @created 20-Sep-2021 10:29:13 AM
 */
public class TimeTriggeredEvent {

	private string command;
	private tring name;
	private int priority = 1;
	private DateTime triggerTime;

	public TimeTriggeredEvent(){

	}

	public void finalize() throws Throwable {

	}

	public string getCommand(){
		return command;
	}

	public tring getName(){
		return name;
	}

	public int getPriority(){
		return priority;
	}

	public DateTime getTriggerTime(){
		return triggerTime;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setCommand(string newVal){
		command = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setName(tring newVal){
		name = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setPriority(int newVal){
		priority = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setTriggerTime(DateTime newVal){
		triggerTime = newVal;
	}

	/**
	 * 
	 * @param name
	 * @param priority
	 * @param triggerTime
	 */
	public void TriggerTimeEvent(string name, int priority, DateTime triggerTime){

	}

}