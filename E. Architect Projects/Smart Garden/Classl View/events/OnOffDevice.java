package events;

import model.Device;

/**
 * @author F86588D
 * @version 1.0
 * @created 20-Sep-2021 10:29:13 AM
 */
public class OnOffDevice extends Device {

	public OnOffDevice(){

	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	/**
	 * 
	 * @param intV1
	 * @param location
	 * @param intV2
	 */
	public OnOffDevice(int intV1, Device Location location, int intV2){

	}

	public void close(){

	}

}