package model;


/**
 * @author F86588D
 * @version 1.0
 * @created 20-Sep-2021 10:29:13 AM
 */
public class XYLocation extends Device Location {

	protected int x;
	protected int y;

	public XYLocation(){

	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	/**
	 * 
	 * @param x
	 * @param y
	 */
	public void XYLocation(int x, int y){

	}

	/**
	 * 
	 * @param name
	 */
	public DeviceLocation(string name){

	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setX(int newVal){
		x = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setY(int newVal){
		y = newVal;
	}

}