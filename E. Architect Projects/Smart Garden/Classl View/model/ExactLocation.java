package model;


/**
 * @author F86588D
 * @version 1.0
 * @created 20-Sep-2021 10:29:13 AM
 */
public class ExactLocation extends Device Location {

	protected double lalitude;
	protected double longitude;

	public ExactLocation(){

	}

	public void finalize() throws Throwable {
		super.finalize();
	}

	/**
	 * 
	 * @param latitude
	 * @param longitude
	 */
	public ExactLocation(double latitude, double longitude){

	}

	/**
	 * 
	 * @param name
	 */
	public void DeviceLocation(string name){

	}

	public double getLalitude(){
		return lalitude;
	}

	public double getLongitude(){
		return longitude;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setLalitude(double newVal){
		lalitude = newVal;
	}

	/**
	 * 
	 * @param newVal
	 */
	public void setLongitude(double newVal){
		longitude = newVal;
	}

}