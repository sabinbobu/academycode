/*
   - functie care va inlocui intr-un fisier text fiecare aparitie
a unui key
    - va returna si nr de inlocuiri realizare
*/

/*
    idee:
    parcurgem fisierul si folosim vom scrie in dest continutul fisierului path
    folosim buf_dest pentru a copia caracterele de pana la urmatorul cuvant care trebuie inclocuit
    apoi vom scrie cuvantul dorit, urmand sa continuam cu celelalte caractere pana la
     urmatorul caz de replace

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#define  _GNU_SOURCE // 


// typedef  - ia un type existent si apoi un label pentru acel tip
// unsigned long long  = uint64 - un alias

typedef unsigned long long uint64;
const size_t N = 1024;
const int LD = strlen(repl);

uint64 replace(const char *path, const char *key, const char *repl, const char *dest){ 
    // validam parametrii de intrare
    if( !path || !key || !repl || !dest){
        errno = EINVAL; // invalid value pentru argumentele functiei;
        return 0;
}

// deschide fisierele( unul pentru citire, celalalt pentru scriere)
    FILE *f = fopen(path, "r");
    FILE *g = fopen(dest, "w");

    if(!f || !g return 0;
// .....
    uint64 count = 0; // nr de  inlocuiri
    
// definim buf_path pentru ce citim, buf_path pentru ce vom scrie    

    char *buf_path = 0;
    char *buf_dest = 0;
    int i1 = 0;
    int i2 = 0;

// alocam memorie pentru cele doua buf_pathe
    buf_path = (char *) malloc(N + 1);
    buf_dest = (char *) malloc(4*N + 1); // +1 pentru \0


    size_t nRead = 0; // nr de bytes cititi

		do{
// citim din fisier
			nRead = fread(buf_path, 1, N, f);
			
			if(nRead == N && buf_path[nRead - 1] != ' '){
				int i = 0;
				for(int i = nRead; i>0 && buf_path[i] != ' '; i--); // daca am citit 4096 si ultimul caracter e spatiu
				buf_path[i + 1] = 0;
				fseek(f, i - nRead, SEEK_CUR); // citim 4096 bytes
			} else{
					buf_path[nRead] = 0;
			}

       char *old_ptr = buf_path;
       size_t n = 0;			
       i2 = 0;

// procesam bufferul de intrare ( presupunem ca am gasit un match)
			for(char *ptr = strcasestr(buf_path, key); ptr != NULL; ptr = strcasestr(ptr, key), n++){
				// count++;
				n = ptr - old_ptr;
				*(buf_dest + i2) = 0;
				strncat(buf_dest, old_ptr, n);
				i2 += n;
				
				
				for(int j=0: j<LD; j++){
				    buf_dest[i2++] = repl[j];
				}
				
				old_ptr = ptr;
				ptr += strlen(key);
				count++;
			}

			for (int j = 0; j < nRead - (old_ptr - buf_path); ++j)
			{
				buf_dest[i2++] = *(old_ptr + j);
			}
			fwrite(buf_dest, 1, i2, g); // scriem in fisier buf_dest
		}while(nRead == N);
		
		fclose(f);
		fclose(g);
		
return count;
}

int main(){


    return 0;
}
