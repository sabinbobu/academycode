// Lucru cu "const"

#include <stdio.h>
#include <stdlib.h>


void f(const char	*name){
	strcpy(name,"Another name"); // char *strcpy(char *dest, const char *src);

}

void g(char *name){
	strcpy(name, "Another name"); // char *strcpy(char *dest, const char *src);
}
int main(){
		const int x = 10;

		const char* const ioana = "Ioana";
		const char *maria = "Maria";

		maria = ioana;
		// ioana = maria
		// ioana++;
		//f(maria); // segmentation fault: pentru ca incercam sa modificam o zona de memorie care este read-only

		//maria[0] = 'm';
		g(maria); // 
	return 0;
}