#include <stdio.h>
#include <string.h>
#include <unistd.h>
//#include <pthread.h>


void print_status(void *unused){

    for(int i=0;i<10;i++){
        printf(".");
        sleep(1);
    }

    printf("\n");
    return NULL;
}

void display(char *arr, int len)
{

    for (int i=0; i<len; i++){
        printf("%c : %d\n", arr[i], arr[i]);

    }
}
int main(){
   // Metoda 1 :
   char name1[4] = {'I','o','n','\0'};
   // Metoda 2:
   char name2[4] = {'I'};
    name2[1]='o';
    name2[2]='n';
    name2[3] = '\0';

    // pentru a fi tratat ca si un sir de caractere, array ul se va termina cu un \0

    printf("size=%ld\n",sizeof(name2));
    printf("strlen=%ld\n", strlen(name2));

    char *ion = "Ion";

    printf("size=%ld\n",sizeof(ion));
    printf("strlen=%ld\n", strlen(ion));

    char vasile[] = "Vasile";
    printf("size=%ld\n",sizeof(vasile));
    printf("strlen=%ld\n", strlen(vasile));

   // pthread_t tid;
   //pthread_create(&tid, NULL, print_status, NULL);
   // pthread_join(tid, NULL);

    display(name1,sizeof(name1));


    return 0;
}
