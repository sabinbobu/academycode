#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct student{
    char *name;
    int *grades;
    int n;
};

void init( struct student *stud, char *name,int *grades, int n){
    stud->name=strdup(name);
    stud->grades=(int*)malloc(n*sizeof(int));
    stud->n = n;
    memcpy(stud->grades,grades, n*sizeof(int));
}

void destroy(struct student *stud){
    free(stud->name);
    free(stud->grades);
}

int *generate_grades(int n)
{
    int *grades = (int*) malloc(n * sizeof(int));
    for (int i = 0; i < n; i++)
        grades[i] = 3 + rand() % 8;
    return grades;
}


// calcul medie
float medie( struct student *s){
    float sum=0.0;
    for(int i=0; i<s->n; i++){
        sum += s->grades[i];
    }
    return sum/s->n;

}

void display_stud(struct student *s){
    printf("Nume: %s, media=%f\n", s->name, medie(s));
}
///-----

int cmpMedie(const void *s1,const void *s2){
   const struct student **std1 = (const struct student**)s1;
   const struct student **std2 = (const struct student**)s2;


    if(medie(*std1) - medie(*std2) > 2.220446049250313e-16 )
        return -1;
    else if(medie(*std2) - medie(*std1) > 2.220446049250313e-16 )
        return 1;
    return 0;
}

void display_best(struct student **studenti,int n){

    qsort(studenti, n, sizeof(struct student*),cmpMedie);

}

int add(int a, int b) { return a + b; }

int main()
{
    int (*op)(int, int) = add;
    char *first_names[] = {"Ion", "Vasile", "Maria", "Ioana"};
    char *last_names[] = {"Ionescu", "Popescu", "Marinescu", "Popa"};

    char full_name[64];
    struct student *studenti[]={
       (struct student*)malloc(sizeof(struct student)),
        (struct student*)malloc(sizeof(struct student)),
         (struct student*)malloc(sizeof(struct student)),
          (struct student*)malloc(sizeof(struct student)),
            (struct student*)malloc(sizeof(struct student)),
    };

    for (int i=0; i<5; i++){
        int n = 5 + rand() % 6;
        int *grades = generate_grades(n);
        int i1 = rand() % 4, i2 = rand() % 4;
        sprintf(full_name, "%s %s", first_names[i1], last_names[i2]);

        init(studenti[i], full_name, grades,n);

        free(grades);
    }

    display_best(studenti,5);

    for(int i=0; i<5; i++){
        display_stud(studenti[i]);
    }

    for(int i=0;i<5;i++){
        destroy(studenti[i]);
        free(studenti[i]);
    }

    return 0;
}
