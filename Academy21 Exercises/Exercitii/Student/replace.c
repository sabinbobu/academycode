#define _GNU_SOURCE
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef unsigned long long uint64;

int isSeparator(char ch){

    return ch == ' ' || ch == '.' || ch == ',' || ch == ':' || ch == ';' || ch == '\t' || ch == '\n' ;
}


uint64 replace(const char *path, const char *key, const char *repl, const char *dest){
    if (!path || !key || !dest || !repl){
        errno = EINVAL;
        return 0;
    }

    FILE *f = fopen (path, "r");
    FILE *g = fopen (dest, "w");

    if (!f || !g)
        return 0;

    uint64 count = 0;
    const size_t N = 1024;
    const int LD = strlen(repl);
    char *buf_path = 0, *buf_dest = 0;
    int i1 = 0, i2 = 0;
    
    buf_path = (char*)malloc(N + 1);
    buf_dest = (char*)malloc(4 * N + 1);

    size_t nRead = 0;
        do {
            nRead = fread(buf_path, 1, N, f);

            if (nRead == N && !isSeparator(buf_path[nRead - 1])){
                 
                int i = 0;
                for(i=nRead;i > 0 && !isSeparator(buf_path[i]);i--);
                    
                 buf_path[i+1] = 0;
                 fseek(f, i - nRead - 1, SEEK_CUR);}
            else {buf_path[nRead] = 0;
            }
            
            char *old_ptr = buf_path;
            size_t n = 0;
            i2 = 0;
            
            for (char *ptr = strcasestr(buf_path, key); ptr != NULL; 
                ptr = strcasestr(ptr, key)){
                n = ptr - old_ptr;
                *(buf_dest + i2)= 0;
                strncat(buf_dest, old_ptr, n);
                i2 += n;
                   
                for (int j=0; j<LD; j++){
                    buf_dest[i2++] = repl[j];
                }
            
                ptr += strlen(key);
                old_ptr = ptr;
                count++;
            }
            
            for (int j=0; j < nRead - (old_ptr - buf_path); j++){
                buf_dest[i2++] = *(old_ptr + j);
            }
            
            fwrite(buf_dest, 1, i2, g); 
        } while (nRead == N); 
    
    fclose(f);
    fclose(g);
    return count;
}


int main(int argc, char **argv)
{

    if (argc < 5)
        exit(-1);
        
    uint64 n = replace(argv[1], argv[2], argv[3], argv[4]);
    printf("%lld\n", n);
    
    
    return 0;
}

