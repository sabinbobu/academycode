#include <stdio.h>
#include <stdlib.h>

void f2();

void f1(){
    printf("f1()\n");
    for(int i=0; i < 1000; i++)
        f2();
}

void f2(){
    int *p = allocate(10);
    f3(p,10);
    free(p);
}

void f3( int *p, int n){
}

int *allocate(int n){
    return (int*)malloc(n*sizeof(int));
}

int main()
{
    f1();
}
