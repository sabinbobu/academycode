#include <stdlib.h>
#include <stdio.h>

int **create(int l, int c )
{
  int **mat=(int**)malloc(l * sizeof(int*));
  if(mat == NULL)
    return NULL;

  for(int i=0;i<l;i++)
  {
  	mat[i]=(int*) malloc (c * sizeof(int));
  	if(mat[i] == NULL){
        for(int j=0; j<i; j++)
            free(mat[j]);
        free(mat);
        return NULL;
  	}

  	 for(int j=0;j<c;j++)
  	 	mat[i][j]=10;
	}
  return mat;
}
void destroy(int **mat, int l){
    if(mat != NULL){
      for(int i=0;i<l;i++){
            if(mat[i] != NULL)
                free(mat[i]);
            mat[i] = NULL;
      }
      free(mat);
    }

}

//////////////// O varinata mai cache friendly /////////////////////

int *create1d(int l, int c){
    int *mat = (int*) malloc(l*c*sizeof(int));
    for(int i=0;i<l;i++){
        for(int j=0;j<c;j++)
        {
            mat[i*c+j] = 10;
        }
    }

    return mat;
}
int main()
{
	int **mat=create(4,5);

	if( mat == NULL ){
        exit(-1);
	}
	for(int i=0;i<4;i++){
        for(int j=0;j<5;j++)
			printf(" %d ",mat[i][j]);
     printf("\n");
	}


	destroy(mat,4);
	mat = NULL;

    printf("\n");printf("\n");

	int *mat1d = create1d(4,5);
	for(int i=0;i<4;i++){
        for(int j=0;j<5;j++)
            printf(" %d ",mat1d[i*5+j]);
    printf("\n");
	}
	free(mat1d);
	return 0;
}
