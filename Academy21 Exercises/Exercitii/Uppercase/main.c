#include <stdio.h>
#include <string.h>
#include <unistd.h>

void Uppercase(char *sir){
    for(int i=0; i<strlen(sir); i++){
        if(sir[i]>='a' && sir[i]<= 'z')
        {
            sir[i]-='a';
            sir[i]+='A';
        }
    }
}

void Uppercase2(char *sir){
    if(sir){
        char *c = sir;
        while(*c){
            if(*c>='a' && *c<= 'z')
                *c-='a'-'A';
            c++;
        }
    }
}

int main(){
   // Metoda 1 :
   char name1[4] = {'I','o','n','\0'};
   // Metoda 2:
   char name2[4] = {'I'};
    name2[1]='o';
    name2[2]='n';
    name2[3] = '\0';

    Uppercase(name1);
    printf("%s\n",name1);

    Uppercase2(name1);
    printf("\n%s\n",name1);
    return 0;
}
