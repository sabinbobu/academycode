#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

struct Faculty;

struct Person{

    char *name;
    unsigned char age;
    struct Faculty *faculty;

};

struct Faculty{
    char *name;
    char *address;
    unsigned int use_count;

};

void register_student(struct Person *p, struct Faculty *f){

    if(p != 0 && f != 0)
        p->faculty = f;
        f->use_count++;

}

void init_person(struct Person *p, const char name, unsigned char age,
                 struct Faculty *f){
    if(p!= NULL)
{
    p->name = strdup(name);
    p->age = age;
    register_student(p, f);
    }
}

void destroy_faculty(struct Faculty *f){
    if(f != NULL){
        if(f->name){
            free(f->name);
            f->name = NULL;
  }
    if(f->address){
    free(f->address);
    f->address = NULL;

}
}


}

void unregister_student(struct Person *p){

     if(p != NULL && p->faculty != NULL){
            p->faculty->use_count--;
         if( p->faculty->use_count == 0) // if there are no students registred
        {
             destroy_faculty(p->faculty);
                free(p->faculty);
         }
           p->faculty = NULL;
     }
}

void destroy_person( struct Person *p){
    if(p != NULL ){
        printf("Destroying person %s\n", p->name);
        unregister_student(p);
        if( p->name ){
            free(p->name);
            p->name = NULL;
        }
     }

}

void init_faculty(struct Faculty *f, const char *name, const char *address){

if(f != NULL ){
    f->name = strdup(name);
    f->address = strdup(address);
    f->use_count = 0;
}




void display_faculty(struct Faculty *f){
    if(f != NULL )
    {
        printf("Facultatea %s, %d studenti\n",f->name,f->use_count);

    }

}

void display_person(struct Person *p)
{
    if(p != NULL){
    printf("%s, %d ani, \n", p->name, p->age);
    display_faculty(p->faculty);
}
}


int main(){
        struct Faculty *ac = (struct Faculty *) malloc(sizeof(struct Faculty));
        assert(ac != NULL);
        init_faculty(ac,"AC", "Str Garii, nr 100, Cj");

        struct Person *ion; // pe heap
        init_person(&ion, "Ion Ionescu", 20, ac); // pe stack

        display_person(&ion);
        destroy_person(ion);
    return 0;
}
