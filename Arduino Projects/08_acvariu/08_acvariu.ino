#include <avr/wdt.h>
//#include <Servo.h>D
//#include <DS18B20.h>
#include <dht.h>

const unsigned BAUD_RATE = 9600;
const unsigned FREQ = 10; // HERTZ


// =====================================
const unsigned NTASKS = 10;

typedef enum TASK_STATE { WAIT, READY, RUN } task_state; 
typedef struct task
{
  unsigned id{0};
  unsigned vtime{};
  task_state state{WAIT};
  void (*f)(void *){};
  void *param{};
  unsigned prio{};
  unsigned T{};  // period
  unsigned D{};  // delay
  unsigned timeout{};
  bool periodic{true};
} task;

task tasks[NTASKS];

// =====================================


void setup_timer1()
{
  cli();
  TCCR1A=0;
  TCCR1B=0;
  TCNT1=0; //SET INIT VALUE

  //set prescaler to 1024
  TCCR1B |= 1 << CS02 | 1 << CS00;

  OCR1A = 16 * pow(10,6) / (1024 * FREQ) - 1;
  
  TIMSK1 |= 1<< OCIE1A;  // ENABLE INTERRUPT FOR OCR1A
  TCCR1B |= 1 << WGM12;  // CTC (clear timer on compare) mode

  sei();  // begin to count
}

unsigned add_task(void (*f)(void*), void *param, unsigned T, unsigned D, unsigned timeout, bool periodic, unsigned prio)
{
  if (!f || T == 0) return NTASKS;
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f == NULL) {
      tasks[i].id = i;
      tasks[i].f = f;
      tasks[i].param = param;
      tasks[i].T = T;
      tasks[i].D = D;
      tasks[i].timeout = timeout;
      tasks[i].periodic = periodic;
      tasks[i].prio = prio;
      tasks[i].vtime = 0;
      tasks[i].state = WAIT;

      return i;
    }
  }
  return NTASKS;  // lista e plina
}

unsigned delete_task(unsigned id)
{
  if (id < NTASKS) {
    tasks[id].f = NULL;
    tasks[id].state = WAIT;
    tasks[id].T = 0;  
  }
}

ISR(TIMER1_COMPA_vect)  // "scheduler tick"
{
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f != NULL) {
      if (tasks[i].state == RUN) {
        if (++tasks[i].vtime > tasks[i].timeout) {
          delete_task(i);  // TODO
        }
      } else {
        if (tasks[i].D == 0) {
          tasks[i].state = READY;
          tasks[i].vtime = 0;
          if (tasks[i].periodic)
            tasks[i].D = tasks[i].T;
        } else {
          tasks[i].D--;
        }
      }
    }
  }
}

void init_scheduler()
{
  setup_timer1();
}

void process_commands(void *);

void setup_watchdog()
{
  cli();
  sei();
}

// ==========================
//BS18B20 temp(2);
//GA1A12S202 lum(A0);
//Servo servo;
dht DHT;
unsigned buttonLightUp = 22;
unsigned buttonLightDown = 23;
unsigned buttonTempUp = 24;
unsigned buttonTempDown = 25;
unsigned heater = 26;
unsigned servo_feed = 27;

unsigned led1 = 12;
unsigned led2 = 11;
unsigned led3 = 10;

volatile float temp;
volatile float temp_threshold = 25;
const float temp_step = 0.5;
volatile float light_threshold = 1000;
const float light_step = 100;

volatile bool light_is_on = false;
volatile bool heat_is_on = false;
volatile bool feed_is_on = false;

void light_up_pressed() { light_threshold += light_step; }
void light_down_pressed() { light_threshold -= light_step; }
void temp_up_pressed() { temp_threshold += temp_step; }
void temp_down_pressed() { temp_threshold -= temp_step; }



#define DHT11_PIN 4
#define DHT21_PIN 5
#define DHT22_PIN 6


void setup_buttons()
{
  pinMode(buttonLightUp, INPUT_PULLUP);
  pinMode(buttonLightDown, INPUT_PULLUP);
  pinMode(buttonTempUp, INPUT_PULLUP);
  pinMode(buttonTempDown, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(buttonLightUp), light_up_pressed, RISING);
  attachInterrupt(digitalPinToInterrupt(buttonLightDown), light_down_pressed, RISING);
  attachInterrupt(digitalPinToInterrupt(buttonTempUp), temp_up_pressed, RISING);
  attachInterrupt(digitalPinToInterrupt(buttonTempDown), temp_down_pressed, RISING);
}

void setup_leds()
{
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);  
}

void setup_servo()
{
  //servo.attach(servo_feed);
  //servo.write(0);  
}

void turn_on_leds()
{
  digitalWrite(led1, HIGH);
  digitalWrite(led2, HIGH);
  digitalWrite(led3, HIGH);
}

void turn_off_leds()
{
  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);
}

void feed_on(void *unused) { Serial.println("Turn on feed"); }
void feed_off(void *unused) { Serial.println("Turn off feed"); }

void feed(void *unused)
{
    add_task(feed_on, NULL, 2 * FREQ, 0, 2 * FREQ, false, 1);
    add_task(feed_off, NULL, 2 * FREQ, 2 * FREQ, 2 * FREQ, false, 1);
}

void update_temp(void *unused)
{
  int chk = DHT.read22(DHT22_PIN);
  if (chk == DHTLIB_OK) {
    temp = DHT.temperature;
    Serial.println(temp);
  } else {
    Serial.println("Error reading temp");  
  }
}

void check_temp(void *unused)
{
  if (temp < temp_threshold) {
    digitalWrite(heater, HIGH);
    Serial.println("Pornesc caldura");
  } else if (temp > temp_threshold + 0.5) {
    digitalWrite(heater, LOW);
    Serial.println("Opresc caldura");
  }
}

void setup()
{
  Serial.begin(BAUD_RATE);
  setup_buttons();
  setup_leds();
  pinMode(heater, OUTPUT);
  //setup_servo();

  // add tasks
  add_task(feed, NULL, 3 * 3600 * FREQ, 0, 3 * FREQ, true, 1);
  add_task(update_temp, NULL, 10 * FREQ, 0, 5 * FREQ, true, 1);
  add_task(check_temp, NULL, 2 * 60 * FREQ, 2 * 60 * FREQ, 1 * FREQ, true, 1);
  
  init_scheduler();
  wdt_enable(WDTO_30MS);
}


void loop()
{
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f != NULL && tasks[i].state == READY) {
      tasks[i].state = RUN;
      tasks[i].f(tasks[i].param);  // execute task (might take longer than expected)
      tasks[i].state = WAIT;
      if (!tasks[i].periodic)
        delete_task(i);
    }
    wdt_reset();
  }
}
