#include <Servo.h>
#include <SPI.h>

const bool is_master = true;
volatile byte led_value = LOW;
volatile bool master_send_ok = true;
volatile bool data_available = false;

const unsigned LEN = 1;
char buf1[LEN], buf2[LEN];

volatile unsigned index = 0;
volatile unsigned to_be_displayed = 1u;
volatile bool ready_to_process = false;

volatile unsigned x = 0;

Servo servo;

void setup_master()
{
  cli();
  SPI.begin();

  pinMode(MOSI, OUTPUT);
  pinMode(MISO, INPUT);
  pinMode(SCK, OUTPUT);
  pinMode(SS, OUTPUT);

  SPCR |= 1 << SPE;   // SPI enable
  SPCR |= 1 << SPIE;  // enable interrupt
  SPCR |= 1 << MSTR;  // this is master => MSTR = 1

  // setez frecventa SPI-ului = 16MHz / 4 = 4MHz
  SPCR &= ~(1 << SPR1);   // SPR1 = 0
  SPCR &= ~(1 << SPR0);   // SPR0 = 0
  SPSR &= ~(1 << SPI2X);  // SPI2X = 0

  SPCR &= ~(1 << DORD);  // DORD = 0 => MSB first

  SPCR &= ~(1 << CPOL);
  SPCR &= ~(1 << CPHA);

  sei();
}

void setup_slave()
{
  cli();
  SPI.begin();

  pinMode(MOSI, INPUT);
  pinMode(MISO, OUTPUT);
  pinMode(SCK, INPUT);
  pinMode(SS, INPUT);

  SPCR |= 1 << SPE;  // enable SPI
  SPCR |= 1 << SPIE; // enable interrupt

  SPCR &= ~(1 << MSTR);  // this is slave => MSTR = 0
  SPCR &= ~(1 << DORD);  // DORD = 0 => MSB first

  SPCR &= ~(1 << CPOL);
  SPCR &= ~(1 << CPHA);

  sei();
}

ISR(SPI_STC_vect)
{
  if (!is_master) {
    char *buf = (to_be_displayed == 1) ? buf2 : buf1;
    buf[index++] = SPDR;  // data register
    led_value = (led_value == HIGH) ? LOW : HIGH;  // toggle led
  
    if (index == LEN) {
      to_be_displayed = (to_be_displayed == 1) ? 2 : 1;
      index = 0;
      ready_to_process = true;
    }
  } else {
    master_send_ok = true;
  }
}


ISR(ADC_vect)
{
  x = ADCL;  // LS 8b
  x += ADCH << 8;  // 0 - 1023
  data_available = true;
}


void setup_adc()
{
  //DIDR0 = 0;
  //DIDR0 |= 1 << ADC0D;
  DIDR0 = B00000001;
  ADMUX = 0;
  ADMUX &= ~(1 << REFS1);  // compare against vcc
  ADMUX |= 1 << REFS0;
  ADMUX &= ~(1 << ADLAR);

  ADCSRB = 0;

  ADCSRA = 0;
  ADCSRA |= 1 << ADEN;
  ADCSRA |= 1 << ADSC;
  ADCSRA |= 1 << ADATE;
  ADCSRA &= ~(1 << ADIF);  // ?
  ADCSRA |= 1 << ADPS2;
  //ADCSRA = B11101100;
  ADCSRA |= 1 << ADIE;
}

void setup()
{
  cli();
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(500000);

  if (is_master) {
    setup_master();
    setup_adc();
  } else {
    setup_slave();
    pinMode(7, OUTPUT);
    servo.attach(7);
  }
  sei();
  servo.write(90);
}

void spi_send(unsigned char value)
{
  digitalWrite(SS, LOW);  // slave select
  master_send_ok = false;
  SPDR = value;
  digitalWrite(SS, HIGH);  
}

void loop()
{
  if (is_master && master_send_ok && data_available) {
    x /= 6;
    digitalWrite(SS, LOW);  // slave select
    master_send_ok = false;
    SPDR = x;
    digitalWrite(SS, HIGH);
    data_available = false;
  } else {
    if (ready_to_process) {
      char *buf = (to_be_displayed == 1) ? buf1 : buf2;
      Serial.println((int)buf[0]);
      servo.write(buf[0]);
      ready_to_process = false;     
    }
  }
}
