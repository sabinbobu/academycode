/*
  Button
  bydefault el este pe HIGH
  cand il apas e pe LOW*/


unsigned button = 3u;     // the number of the pushbutton pin
volatile int value = HIGH;
volatile bool changed = false;
const int led = 13;

void on_pressed() {
  value = digitalRead(button);
  changed = true;
}
void setup() {
  Serial.begin(9600);
  // initialize the pushbutton pin as an input:
  pinMode(button, INPUT);
  // initialize the LED pin as an output:
  pinMode(led , OUTPUT);

  attachInterrupt(digitalPinToInterrupt(button), on_pressed, CHANGE);

}

void loop() {
  // read the state of the pushbutton value:
  value = digitalRead(button);
  Serial.println(value ? "Aprins" : "Stins");
  delay(1000);

  if (changed) {
    Serial.println(value ? "Aprins" : "Stins");
    changed = false;
  }




    // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
    if (value == HIGH) {
      // turn LED on:
      digitalWrite(led, HIGH);
    } else {
      // turn LED off:
      digitalWrite(led, LOW);
    }
}
