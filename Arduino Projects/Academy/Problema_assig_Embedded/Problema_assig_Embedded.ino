/*
  Implement a simple system for taking care of the fish inside an aquarium.
  The system monitors the temperature (DS18B20) and the light conditions (GA1A12S202).

  The system feeds the fish every 3 hours.
  It does so by activating a servo from 0 to 30 degrees for 2 seconds (we don't care about the mechanical aspects).

  In case the lighting is bellow a programmable threshold (defaults to 1000 Lux), the system shall power 3 LEDs.
  When the light intensity is above the threshold (you must quickly turn off the LEDs for this), the LEDs shall be turned off.
  Checking the light intensity and temperature is done every 2 minutes.

  In case the temperature falls below a programmable threshold, the system shall activate a heater (we simply turn on a relay for this one).
  When the temperature == threshold + 0.5 C, the relay is turned off.
  Note: the relay can be connected in the normally open mode <=>
  turning on the heater means setting the digital output pin to HIGH.

  There are fours buttons for programming the temperature and humidity levels:
  each press of the light intensity buttons increases / decreases the threshold with 100 Lux.
  Each press of the temperature button increases / decreases the temperature threshold with 0.5 degrees.

  -------------------------------------------------------- */
// - - - - - - DHT22  - - -- - - - - -
//Libraries
#include <DHT.h>
//Constants
#define DHTPIN 9     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

//Variables
int chk;
float hum;  //Stores humidity value
float temp; //Stores temperature value
//--------------------------------------------
//
// ------------------ Servo -------------------------------
#include <Servo.h>
int servoPin = 10;
Servo servo;
int servoAngle = 0;   // servo position in degrees


//  ----------------- variabile globale -------------
volatile int seconds_2min = 0; //make it volatile because it is used inside the interrupt
volatile int seconds_2sec = 0;

struct ledcontrol;
// unsigned T = 21;
unsigned L = 1001;

// - - - - Butoane
const int buttonPin2 = 2;
const int buttonPin3 = 3;
int buttonState2 = LOW;
int buttonState3 = LOW;

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

// functii
void start_servo();
void check_light();
void check_temp();
void read_temp();
void read_button_state();

//void empty( struct ledcontrol*)
void ledon(struct ledcontrol*);
void ledoff(struct ledcontrol*);
void blink_led(struct ledcontrol*);

const long int BAUD_RATE = 115200;
const unsigned FREQ = 1; // HERTZ

typedef enum STATE {BLINK = 0, LEDON, LEDOFF, START, STOP} state;

void (*operations[])(struct ledcontrol*) = {ledon, ledoff, blink_led, ledoff};


typedef struct ledcontrol
{
  volatile unsigned int delay1;
  volatile unsigned int delay2;
  volatile unsigned int delay_on;
  volatile unsigned int delay_off;
  unsigned int port;
  volatile unsigned value;
  state current;
  state desired_next;

} ledcontrol;

ledcontrol led1 = {0, 0, 0, 0, 6, LOW, LEDOFF, LEDOFF};
ledcontrol led2 = {0, 0, 0, 0, 7, LOW, LEDOFF, LEDOFF};
ledcontrol led3 = {0, 0, 0, 0, 8, LOW, LEDOFF, LEDOFF};


void setup_timer0()
{
  cli();
  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0 = 0; //SET INIT VALUE

  
  TCCR0A=(1<<WGM01);    //Set the CTC mode   
  OCR0A=0xF9; //Value for ORC0A for 1ms 
  
  TIMSK0|=(1<<OCIE0A);   //Set the interrupt request
  
  TCCR0B|=(1<<CS01);    //Set the prescale 1/64 clock
  TCCR0B|=(1<<CS00);
  

  sei();  // begin to count

}




ISR(TIMER0_COMPA_vect) { //Interrupt Service Routine, Timer/Counter1 Compare Match A
  seconds_2min++;
  seconds_2sec++;
}
void init_scheduler()
{
  setup_timer0();
}

void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(led1.port, OUTPUT);
  pinMode(led2.port, OUTPUT);
  pinMode(led3.port, OUTPUT);

  dht.begin();

  pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);

  servo.attach(servoPin);
  init_scheduler();
}

void empty(ledcontrol* led) {}
void ledon(ledcontrol* led) {
  digitalWrite(led->port, HIGH);
}
void ledoff(ledcontrol* led) {
  digitalWrite(led->port, LOW);
}
void blink_led(ledcontrol* led) {
  digitalWrite(led->port, led->value);
}

void check_light() {
  if (L > 1000) {
    ledon(&led1);
    ledon(&led2);
    ledon(&led3);
  } else {
    ledoff(&led1);
    ledoff(&led2);
    ledoff(&led3);
  }
}
void check_temp() {

  if (temp < 22 ) {
    Serial.println("Releu activat.");
  }
  else {
    Serial.println("Releu dezactivat.");
  }

}

void read_temp() {
  delay(10);
  //Read data and store it to variables hum and temp
  hum = dht.readHumidity();
  temp = dht.readTemperature();
  //Print temp and humidity values to serial monitor
  Serial.print("-- > Temp: ");
  Serial.print(temp);
  Serial.println(" Celsius");
}

void read_button_state() {
  buttonState2 = digitalRead(buttonPin2); // +
  buttonState3 = digitalRead(buttonPin3); // -

  if ( (millis() - lastDebounceTime) > debounceDelay) {
    if (buttonState2 == HIGH) {
      L += 100;
    }
  }

  if ( (millis() - lastDebounceTime) > debounceDelay) {
    if (buttonState3 == HIGH) {
      L -= 100;
    }
  }

  Serial.print("Lumina: "); Serial.println(L);

} // end read_button_state

void loop() {
  read_temp();
  read_button_state();

  if (seconds_2min >= 2000) { //o data la 2 min = 120 sec (120 000 ms )
    Serial.println("------ New Check ---------");           // This code is what happens
    check_light();
    check_temp();
    seconds_2min = 0;                        // after 'x' seconds
  }

  if (seconds_2sec <= 2000) // activare servo pentru 2 sec ( 2000 ms )
  {
    for (servoAngle = 0; servoAngle < 30; servoAngle++) //move the micro servo from 0 degrees to 180 degrees
    {
      servo.write(servoAngle);
    }
    for (servoAngle = 30; servoAngle >= 0; servoAngle--) //now move back the micro servo from 0 degrees to 180 degrees
    {
      servo.write(servoAngle);
    }

    seconds_2sec = 0;
  }
  


  
}
