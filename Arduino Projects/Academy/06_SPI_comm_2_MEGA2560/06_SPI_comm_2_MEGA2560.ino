#include <SPI.h>

const bool is_master = false;
volatile unsigned char led_value = LOW;

const unsigned LEN = 1;
char buf1[LEN + 1], buf2[LEN + 1];
volatile unsigned index = 0;
volatile unsigned to_be_displayed = 1u;
volatile bool ready_to_display = false;

void setup_master() {
  SPI.begin();

  pinMode(MOSI, OUTPUT); // Master out Slave in -> aici dam comanda
  pinMode(MISO, INPUT); // Master in Slave out
  pinMode(SCK, OUTPUT);
  pinMode(SS, OUTPUT); // activ pe 0

  // setam intreruperea
  //SPI.attachInterrupt();

  SPCR |= 1 << SPE; // SPI Enable
  SPCR |= 1 << SPIE; // SPI  Interrupt Enable
  SPCR |= 1 << MSTR; // cine e masterul

  // setez frecventa SPI ului = 16MHz / 128 = 125 KHz ( in reg de Control )
  SPCR |= 1 << SPR1; // punem 1
  SPCR |= 1 << SPR0; // punem 1
  SPSR |= 1 << SPI2X; // setam in Status register SPI2X ca sa ne asiguram ca alegem divizorul de 128

  SPCR &= ~(1 << DORD ); // reset DORD = > MSB first
  SPCR &= ~(1 << CPHA );
  sei(); // set enable interrupt

}

void setup_slave() {
  cli();

  SPI.begin();
  // SPI.setClockDivider(SPI_CLOCK_DIV128);
  // SPI.setBitOrder(MSBFIRST);

  pinMode(MOSI, INPUT); // Master out Slave in -> aici dam comanda
  pinMode(MISO, OUTPUT); // Master in Slave out
  pinMode(SCK, INPUT);
  pinMode(SS, INPUT);

  // setam intreruperea
  // SPI.attachInterrupt();

  SPCR |= 1 << SPE; // SPI Enable
  SPCR |= 1 << SPIE; // SPI  Interrupt Enable

  // SPI Control Register
  SPCR &= ~(1 << MSTR); // cine e masterul
  SPCR &= ~(1 << DORD ); // reset DORD = > MSB first

  SPCR &= ~(1 << CPOL);
  SPCR &= ~(1 << CPHA );

  sei(); // set enable interrupt


}

ISR(SPI_STC_vect) {

  char *buf = to_be_displayed == 1 ? buf2 : buf1;

  buf[index++] = SPDR; // datele venite dpe SPI le punem in SPDR & schimbam bufferul

  led_value = led_value == HIGH ? LOW : HIGH; // toggle led

  if (index == LEN ) {
    to_be_displayed = to_be_displayed  == 1 ? 2 : 1; // facem toggle
    index = 0;
    ready_to_display = true;
  }
}

void setup() {
  if (is_master)
    setup_master();
  else
    setup_slave();

  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(115200); // viteza mai mare de citire scriere
}

int i=1;
void loop() {

  if (is_master) { // daca trimite MASTERul
    digitalWrite(SS, LOW); // selectam SLAVEul
    SPDR = 'X';
    Serial.print(i);
    Serial.println(" : trimis");
    i++;
    delay(500);
     //digitalWrite(SS, HIGH); // dezactivam SLAVEul
  }
  else { // daca
    digitalWrite(LED_BUILTIN, led_value);
    Serial.println("Am primit");
    if (ready_to_display) {
      char *buf = to_be_displayed == 1 ? buf1 : buf2;
      buf[LEN] = 0; // il facem string
      Serial.println(buf); // Afisam
      ready_to_display = false;

    }
  }
}
