#include <SPI.h>

const bool is_master = false;
volatile byte led_value = LOW;
volatile bool master_send_ok = true;

const unsigned LEN = 32;
char buf1[LEN + 1], buf2[LEN + 1];

volatile unsigned index = 0;
volatile unsigned to_be_displayed = 1u;
volatile bool ready_to_display = false;

void setup_master()
{
  cli();
  SPI.begin();

  pinMode(MOSI, OUTPUT);
  pinMode(MISO, INPUT);
  pinMode(SCK, OUTPUT);
  pinMode(SS, OUTPUT);

  SPCR |= 1 << SPE;   // SPI enable
  SPCR |= 1 << SPIE;  // enable interrupt
  SPCR |= 1 << MSTR;  // this is master => MSTR = 1

  // setez frecventa SPI-ului = 16MHz / 4 = 4MHz
  SPCR &= ~(1 << SPR1);   // SPR1 = 0
  SPCR &= ~(1 << SPR0);   // SPR0 = 0
  SPSR &= ~(1 << SPI2X);  // SPI2X = 0

  SPCR &= ~(1 << DORD);  // DORD = 0 => MSB first

  SPCR &= ~(1 << CPOL);
  SPCR &= ~(1 << CPHA);

  sei();
}

void setup_slave()
{
  cli();
  SPI.begin();

  pinMode(MOSI, INPUT);
  pinMode(MISO, OUTPUT);
  pinMode(SCK, INPUT);
  pinMode(SS, INPUT);

  SPCR |= 1 << SPE;  // enable SPI
  SPCR |= 1 << SPIE; // enable interrupt
  
  SPCR &= ~(1 << MSTR);  // this is slave => MSTR = 0
  SPCR &= ~(1 << DORD);  // DORD = 0 => MSB first

  SPCR &= ~(1 << CPOL);
  SPCR &= ~(1 << CPHA);

  sei();
}

ISR(SPI_STC_vect)
{
  if (!is_master) {
    char *buf = (to_be_displayed == 1) ? buf2 : buf1;
    buf[index++] = SPDR;  // data register
    buf[index] = 0;
    led_value = (led_value == HIGH) ? LOW : HIGH;  // toggle led
  
    if (index == LEN) {
      to_be_displayed = (to_be_displayed == 1) ? 2 : 1;
      index = 0;
      ready_to_display = true;
    }
  } else {
    master_send_ok = true;
  }
}

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(500000);

  if (is_master)
    setup_master();
  else
    setup_slave();
}

void loop()
{
  if (is_master && master_send_ok) {
    digitalWrite(SS, LOW);  // slave select
    master_send_ok = false;
    SPDR = 'Y';
    digitalWrite(SS, HIGH);
  } else {
    if (ready_to_display) {
      char *buf = (to_be_displayed == 1) ? buf1 : buf2;
      buf[LEN] = 0;
      Serial.println(buf);
      ready_to_display = false;     
      digitalWrite(LED_BUILTIN, led_value); 
    }
  }
}
