// ledon:13
// ledoff::13
// blink:1000:2000:13 delay_on:delay_of:nr_port
// start:13
// start:13
struct ledcontrol;

void empty(struct ledcontrol*);
void ledon(struct ledcontrol*);
void ledoff(struct ledcontrol*);
void blink_led(struct ledcontrol*);


char buf[100];
int index=0;

const unsigned BAUD_RATE=9600;
const unsigned FREQ=1000; //HERTZ

typedef enum STATE {BLINK, LEDON, LEDOFF, START, STOP} state;

void (*operations[])(struct ledcontrol*) = {empty, ledon, ledoff, blink_led, ledoff};

typedef struct transition
{
  state current;
  state next;
  
} transition;

transition transitions[]={{BLINK, START}, {BLINK, STOP}, {LEDOFF, LEDON}, {LEDON, LEDOFF}, {STOP, START}, {START, STOP}, {STOP, LEDON}, {LEDON, BLINK}, {LEDOFF, BLINK}};

typedef struct ledcontrol
{
  unsigned int delay1;
  unsigned int delay2;
  unsigned int delay_on;
  unsigned int delay_off;
  unsigned int port;
  volatile unsigned value;
  state current;
  state desired_next;
  
}ledcontrol;

ledcontrol led1={0,0,0,0,LED_BUILTIN,LOW,LEDON,LEDOFF};
ledcontrol led2={0,0,0,0,9,LOW,LEDOFF,LEDON};

void setup_timer1()
{
  cli();
  TCCR1A=0;
  TCCR1B=0;
  TCNT1=0; //SET INIT VALUE
//set prescaler to 64
TCCR1B |= 1<< CS11;
TCCR1B |= 1<< CS10;

  OCR1A= 16* pow(10,6) / (64*FREQ) - 1;
  
  TIMSK1 |=1<< OCIE1A;  //ENABLE INTERRUPT FOR OCR1A
  TCCR1B |= 1<< WGM12; //CTC (clear timer on compare) mode
  
  sei();  ///begin to count
}

ISR(TIMER1_COMPA_vect)
{
  if (led1.current==START)
  {
    if(--led1.delay1>0)
    {
      led1.value=HIGH;
    }else
    {
      led1.value=LOW;
      if(--led1.delay2==0)
      {
        led1.delay1=led1.delay_on;
        led1.delay2=led1.delay_off;
      }
    }
  }
  
}

void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(led1.port, OUTPUT);
  pinMode(led2.port, OUTPUT);
  setup_timer1();

}

void empty(ledcontrol* led){}
void ledon(ledcontrol* led) {digitalWrite(led->port, HIGH);}
void ledoff(ledcontrol* led) {digitalWrite(led->port, LOW);}
void blink_led(ledcontrol* led) {
  digitalWrite(led->port,led->value);
  }

void read_cmd (char c)
{
  if(isalnum(c) || c == ':' || c=='.')
    buf[index++]=c;
}
void parse_cmd()
{
  ledcontrol *led;
  unsigned port;
  boolean isvalid=false;
  buf[index]='\0';

  if(strstr(buf, "blink")==buf){
    unsigned delay1, delay2;
    sscanf(buf+6, "%u:%u:%u", &delay1, &delay2, &port);
    led = port == LED_BUILTIN ? &led1 : &led2;
    led.desired_next=BLINK;
    led.port = port;
    led.delay1 = delay1;
    led.delay2 = delay2;
    led.delay_on=delay1;
    led.delay_off=delay2;
    isvalid=true;
  }
   if(strstr(buf, "ledon")==buf){
    sscanf(buf+6 , "%u", &port);
    led = port == LED_BUILTIN ? &led1 : &led2;
    *led.desired_next=LEDON;
    *led.port = port;
    isvalid=true;
  }
   if(strstr(buf, "ledoff")==buf){
    sscanf(buf+7 , "%u", &port);
    led = port == LED_BUILTIN ? &led1 : &led2;
    *led.desired_next=LEDOFF;
    *led.port = port;
    isvalid=true;
  }
   if(strstr(buf, "start")==buf){
    sscanf(buf+6 , "%u", &port)
    led = port == LED_BUILTIN ? &led1 : &led2;;
    *led.desired_next=START;
    *led.port = port;
    isvalid=true;
  }
  if(strstr(buf, "stop")==buf){
    sscanf(buf+5 , "%u", &port);
    *led = port == LED_BUILTIN ? &led1 : &led2;
    *led.desired_next=STOP;
    isvalid=true;
     
  }
  if(isvalid){
    
    pinMode(led.port, OUTPUT);
  }
  //to-do error if isvalid 
  index=0;
  Serial.println(buf);
}

void loop() {
  if(Serial.available() > 0) {
    read_cmd(Serial.read());
 
  if (buf[index-1] == '.') {
    parse_cmd();
 
   }
  }
  bool transition_is_valid = false;

  for(int i=0;i<sizeof(transitions)/sizeof(transition);i++)
  {
    if(transitions[i].current == led1.current && transitions[i].next == led1.desired_next) {
      transition_is_valid= true;
      Serial.println("isValid");
      break;
    }

  }
  if(transition_is_valid){
    led1.current = led1.desired_next;
    led2.current = led2.desired_next;
  }
  operations[led1.current](&led1);
  operations[led2.current](&led2);
}
