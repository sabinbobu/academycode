// ledon: 13.
// ledoff: 13.
// blink: 1000:2000:13 delay_on:delay_off:nr_port
//start: 13.
//stop: 13.

void empty();
void ledon();
void ledoff();
void blink_led();

char buf[100];
int delay1 = 0, delay2 = 0; // in ms
int delay_on = 0;
int delay_off = 0;

int port = LED_BUILTIN;
volatile unsigned value = LOW;

const unsigned BAUD_RATE = 9600;
int index;
const unsigned FREQ = 1000; // = 1000 Hz

typedef enum STATE {BLINK=0, LEDON, LEDOFF, START, STOP} state;

void (*operations[])(void) = {empty, ledon, ledoff, blink_led, ledoff};

typedef struct transition
{
  state current;
  state next;  
}transition;

// Automat de stare ( current state, next state )  - STATE MACHINE
// abstractizeaza behaviour
transition transitions[] = {{BLINK, START}, {STOP, START}, {START, STOP}, {LEDOFF, LEDON}, {LEDON, LEDOFF}, {STOP, LEDON}, {LEDON, BLINK}, {LEDOFF, BLINK}};

state current = LEDOFF;
state desired_next = LEDOFF;

void setup_timer(){
  cli();
  TCCR1A = 0;
 
  // frecv de baza
  // setare prescale la 64 
  TCCR1B = 0;
  
  TCCR1B |= 1 << CS11;
  TCCR1B |= 1 << CS10;
  
  OCR1A = 16 * pow(10,6) / (64 * FREQ) -1 ; // punem valoarea ( frecventa uprocesorului este 16MHz  ); -1 pentru ca inecepem numaratoarea de la 1
  // 64 * 1000 = frecventa de prescale
  TIMSK1 |= 1 << OCIE1A; // enable interrupt for OCR1A
  TCCR1B |= 1 << WGM12; // CTC ( clear timer on compare ) mode
  
  sei(); // begin to count
}

// nu executam bussiness logic 
//( cum ar fi aprinderea sau stingerea LED ului ) 
// nu stam mult in ISR
ISR(TIMER1_COMPA_vect){ // Interrupt Service routine pt timer
  if(current == START){
    if(--delay1 > 0){
      value = HIGH;
    }
    else{
      value = LOW;
      if(--delay2 == 0){
        delay1 = delay_on;
        delay2 = delay_off;
      }
    }
  }
}

void setup() {
  // put your setup code here, to run once:
   Serial.begin(BAUD_RATE);
  pinMode(port, OUTPUT);

  setup_timer(); // setam timer ul din datasheet
}

void read_cmd(char c){
  if(isalnum(c) || c == ':' || c == '.')
      buf[index++] = c;
}

// puteam sa facem si cu strsplit s- > obtineam niste token uri intr-un array
void parse_cmd()
{

  boolean isvalid=false;
  buf[index]='\0';
  if(strstr(buf, "blink")==buf){
    desired_next=BLINK;
    sscanf(buf+6, "%u:%u:%u", &delay1, &delay2, &port);
    delay_on = delay1;
    delay_off = delay2;
    isvalid=true;
  }
   if(strstr(buf, "ledon")==buf){
    desired_next=LEDON;
    sscanf(buf+6 , "%u", &port);
     isvalid=true;
    
  }
   if(strstr(buf, "ledoff")==buf){
    desired_next=LEDOFF;
    sscanf(buf+7 , "%u", &port);
     isvalid=true;
  }
   if(strstr(buf, "start")==buf){
    desired_next=START;
    sscanf(buf+6 , "%u", &port);
     isvalid=true;
  }
  if(strstr(buf, "stop")==buf){
    desired_next=STOP;
    sscanf(buf+5 , "%u", &port);
     isvalid=true;
     
  }
  if(isvalid){
    pinMode(port, OUTPUT);
  }
  //to-do error if isvalid 
  index=0;
  Serial.println(buf);
}

void empty(){}
void ledon(){digitalWrite(port, HIGH);}
void ledoff(){digitalWrite(port, LOW); }
void blink_led(){ digitalWrite(port, value); } // cu value vom putea seta valoarea dintr un interrupt ( low or high )

void loop() {
  // urmarim sa nu stam intr-o stare de wait in cadrul functiei loop()
  if(Serial.available() > 0){ // avem ceva pe Serial?
    read_cmd(Serial.read()); // citim cate un caracter ( care vin asincron ) - > citeste continuu cu functia read_cmd()
  
  if( buf[index - 1] == '.'){ // Am ajuns la capatul comenzii?
    parse_cmd();
    
  }
}

  bool transitions_is_valid = false;
  // if transition is_valid, do transition
  // poate face tranzitie sau nu

  for(int i=0; i < (sizeof(transitions)/sizeof(transition)); i++){ // - de la 0 la nr de tranzitii ( n )
    if(transitions[i].current == current && transitions[i].next == desired_next){
      transitions_is_valid = true;
      break;
    }
  }

  if(transitions_is_valid){
    current = desired_next; // do the transition
  }

  operations[current]();

}
