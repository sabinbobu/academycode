//initialize and declare variables
const int A= 13; //led attached to this pin
const int R = 8;
const int G = 7;
const int buttonPin = 3; //push button attached to this pin

int buttonState = LOW; //this variable tracks the state of the button, low if not pressed, high if pressed
int ledState = -1; //this variable tracks the state of the LED, negative if off, positive if on

long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

void setup() {

  //set the mode of the pins...
  pinMode(A, OUTPUT);
  pinMode(R, OUTPUT);
  pinMode(G, OUTPUT);
  pinMode(buttonPin, INPUT);

}//close void setup

void loop() {

  //sample the state of the button - is it pressed or not?
  buttonState = digitalRead(buttonPin);

  //filter out any noise by setting a time buffer
  if ( (millis() - lastDebounceTime) > debounceDelay) {

    //if the button has been pressed, lets toggle the LED from "off to on" or "on to off"
    if ( (buttonState == HIGH) && (ledState < 0) ) { // albastru -> galben

      digitalWrite(A, HIGH); //turn LED on
      delay(500);
      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, HIGH); //turn LED on
      
      delay(500);

      digitalWrite(R, LOW); //turn LED on
      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, HIGH); //turn LED off
      
      delay(500);


      ///////////////

      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, HIGH); //turn LED on
 
      delay(500);

      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, LOW); //turn LED on
      digitalWrite(A, HIGH); //turn LED off
      
      delay(500);
   
      ledState = -ledState; //now the LED is on, we need to change the state
      lastDebounceTime = millis(); //set the current time
    }
    else if ( (buttonState == HIGH) && (ledState > 0) ) { // galben -> albastru

 digitalWrite(A, HIGH); //turn LED on
      delay(500);
      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, HIGH); //turn LED on
      
      delay(500);

      digitalWrite(R, LOW); //turn LED on
      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, HIGH); //turn LED off
      
      delay(500);


      ///////////////

      digitalWrite(A, LOW); //turn LED off
      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, HIGH); //turn LED on
 
      delay(500);

      digitalWrite(G, LOW); //turn LED off
      digitalWrite(R, LOW); //turn LED on
      digitalWrite(A, HIGH); //turn LED off
      
      delay(500);
   
      ledState = -ledState; //now the LED is off, we need to change the state
      lastDebounceTime = millis(); //set the current time
    }

  }//close if(time buffer)

}//close void loop
