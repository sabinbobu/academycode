// ledon:13
// ledoff::13
// blink:1000:2000:13 delay_on:delay_of:nr_port
// start:13
// start:13
#include <avr/wdt.h>

struct ledcontrol;

void empty(struct ledcontrol*);
void ledon(struct ledcontrol*);
void ledoff(struct ledcontrol*);
void blink_led(struct ledcontrol*);
void stop_blinking(struct ledcontrol*);

const unsigned LEN = 100;
char buf[LEN];
int index=0;

const unsigned BAUD_RATE = 9600;
const unsigned FREQ = 1000; // HERTZ

typedef enum STATE {BLINK, LEDON, LEDOFF, START, STOP} state;

void (*operations[])(struct ledcontrol*) = {empty, ledon, ledoff, blink_led, stop_blinking};

typedef struct transition
{
  state current;
  state next;

} transition;

transition transitions[] = {{BLINK, START}, {BLINK, STOP}, {START, STOP}, {STOP, START},
                            {LEDOFF, LEDON}, {LEDON, LEDOFF},
                            {STOP, LEDON}, {LEDON, BLINK}, {LEDOFF, BLINK}};

typedef struct ledcontrol
{
  volatile unsigned int delay_on;
  volatile unsigned int delay_off;
  unsigned int port;
  state current;
  state desired_next;
  
} ledcontrol;

ledcontrol led1={0, 0, LED_BUILTIN, LEDOFF, LEDOFF};
ledcontrol led2={0, 0, 9, LEDOFF, LEDOFF};


// =====================================
const unsigned NTASKS = 10;
unsigned led_on_id = NTASKS;
unsigned led_off_id = NTASKS;

typedef enum TASK_STATE { WAIT, READY, RUN } task_state; 

typedef struct task
{
  unsigned id{0};
  unsigned vtime{};
  task_state state{WAIT};
  void (*f)(void *){};
  void *param{};
  unsigned prio{};
  unsigned T{};  // period
  unsigned D{};  // delay
  unsigned timeout{};
  bool periodic{true};
} task;

task tasks[NTASKS];

// =====================================



void setup_timer1()
{
  cli();
  TCCR1A=0;
  TCCR1B=0;
  TCNT1=0; //SET INIT VALUE

  //set prescaler to 64
  TCCR1B |= 1 << CS01 | 1 << CS00;

  OCR1A = 16 * pow(10,6) / (64 * FREQ) - 1;
  
  TIMSK1 |= 1<< OCIE1A;  // ENABLE INTERRUPT FOR OCR1A
  TCCR1B |= 1 << WGM12;  // CTC (clear timer on compare) mode

  sei();  // begin to count
}

unsigned add_task(void (*f)(void*), void *param, unsigned T, unsigned D, unsigned timeout, bool periodic/*, unsigned prio*/)
{
  if (!f || T == 0) return NTASKS;
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f == NULL) {
      tasks[i].id = i;
      tasks[i].f = f;
      tasks[i].param = param;
      tasks[i].T = T;
      tasks[i].D = D;
      tasks[i].timeout = timeout;
      tasks[i].periodic = periodic;
      //tasks[i].prio = prio;
      tasks[i].vtime = 0;
      tasks[i].state = WAIT;
      return i;
    }
  }
  return NTASKS;  // lista e plina
}

unsigned delete_task(unsigned id)
{
  if (id < NTASKS) {
    tasks[id].f = NULL;
    tasks[id].state = WAIT;
    tasks[id].T = 0;  
  }
}

ISR(TIMER1_COMPA_vect)  // "scheduler tick"
{
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f != NULL) {
      if (tasks[i].state == RUN) {
        if (++tasks[i].vtime > tasks[i].timeout) {
          delete_task(i);  // TODO
        }
      } else {
        if (tasks[i].D == 0) {
          tasks[i].state = READY;
          tasks[i].vtime = 0;
          if (tasks[i].periodic)
            tasks[i].D = tasks[i].T;
        } else {
          tasks[i].D--;
        }
      }
    }
  }
}

void init_scheduler()
{
  setup_timer1();
}

void process_commands(void *);

void setup_watchdog(){
  cli();


  sei();
}

void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(led1.port, OUTPUT);
  pinMode(led2.port, OUTPUT);

  // add tasks
  add_task(ledon, (void*)&led1, 500, 0, 3, true);
  add_task(ledoff, (void*)&led1, 500, 250, 3, true);
  add_task(process_commands, NULL, 10, 2, 5, true);

  init_scheduler();
  // wdt_enable(WDT0_2s);
}

void empty(ledcontrol* led){}
void ledon(ledcontrol* led) { digitalWrite(led->port, HIGH); }
void ledoff(ledcontrol* led) { digitalWrite(led->port, LOW); }

void blink_led(ledcontrol* led)
{
  unsigned T = led->delay_on + led->delay_off;
  led_on_id = add_task(ledon, led, T, 0, 3, true);
  led_off_id = add_task(ledoff, led, T, led->delay_on, 3, true);
}

void stop_blinking(ledcontrol* led)
{
  delete_task(led_on_id);
  delete_task(led_off_id);
  ledoff(led);
}

void read_cmd (char c)
{
  if(isalnum(c) || c == ':' || c=='.')
    buf[index++ % LEN] = c;
}

void parse_cmd()
{
  ledcontrol *led;
  unsigned port;
  boolean isvalid=false;
  buf[index % LEN] = '\0';

  if(strstr(buf, "blink")==buf) {
    unsigned delay1, delay2;
    sscanf(buf+6, "%u:%u:%u", &delay1, &delay2, &port);
    led = (port == LED_BUILTIN) ? &led1 : &led2;
    led->desired_next=BLINK;
    led->port = port;
    led->delay_on=delay1;
    led->delay_off=delay2;
    isvalid=true;
  }
   if(strstr(buf, "ledon")==buf){
    sscanf(buf+6 , "%u", &port);
    led = (port == LED_BUILTIN) ? &led1 : &led2;
    led->desired_next=LEDON;
    led->port = port;
    isvalid=true;
  }
   if(strstr(buf, "ledoff")==buf){
    sscanf(buf+7 , "%u", &port);
    led = (port == LED_BUILTIN) ? &led1 : &led2;
    led->desired_next=LEDOFF;
    led->port = port;
    isvalid=true;
  }
   if(strstr(buf, "start")==buf){
    sscanf(buf+6 , "%u", &port);
    led =( port == LED_BUILTIN) ? &led1 : &led2;
    led->desired_next=START;
    led->port = port;
    isvalid=true;
  }
  if(strstr(buf, "stop")==buf){
    sscanf(buf+5 , "%u", &port);
    led =( port == LED_BUILTIN) ? &led1 : &led2;
    led->desired_next=STOP;
    led->port = port;
    isvalid=true;
     
  }

  if(isvalid)
    pinMode(led->port, OUTPUT);

  //to-do error if isvalid 
  index=0;
  Serial.println(buf);
}

void switch_state(ledcontrol *led)
{
  for(int i = 0; i < sizeof(transitions) / sizeof(transition); i++) {
    if(transitions[i].current == led->current && transitions[i].next == led->desired_next) {
      led->current = led->desired_next;
      Serial.println("isvalid");
      break;
    }
  }
}


void process_commands(void *)
{
  if (Serial.available() > 0) {
    read_cmd(Serial.read());      
    if (buf[index-1] == '.') {
      parse_cmd();
      switch_state(&led2);
      operations[led2.current](&led2);
    }
  }
}


void loop() {
  for (unsigned i = 0; i < NTASKS; i++) {
    if (tasks[i].f != NULL && tasks[i].state == READY) {
      tasks[i].state = RUN;
      tasks[i].f(tasks[i].param);  // execute task (might take longer than expected)
      tasks[i].state = WAIT;
      if (!tasks[i].periodic)
        delete_task(i);
    }

     wdt_reset(); // Watch dog reset
  }
}
