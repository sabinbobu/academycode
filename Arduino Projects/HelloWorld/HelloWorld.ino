//Libraries
#include <LiquidCrystal.h>
#include <DHT.h>

#define SIGNAL_PIN 13

//Constants
#define DHTPIN 7     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

//Variables
int chk;
float hum;  //Stores humidity value
float temp; //Stores temperature value

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  lcd.begin(16, 2);
  lcd.print("    Magneti  ");
  lcd.setCursor(0,1);
  lcd.print("     Marelli   ");
  pinMode(SIGNAL_PIN,INPUT);
  Serial.begin(9600);
  dht.begin();
  
}


void loop() {
  
   delay(2000);
    //Read data and store it to variables hum and temp
    hum = dht.readHumidity();
    temp= dht.readTemperature();
    //Print temp and humidity values to serial monitor
    Serial.print("Humidity: ");
    Serial.print(hum);
    Serial.print(" %, Temp: ");
    Serial.print(temp);
    Serial.println(" Celsius");
    delay(10000); //Delay 2 sec.
    lcd.clear();

  lcd.begin(16, 2);
  lcd.print("H: ");  lcd.print(hum);lcd.print("T: ");  lcd.print(temp);
  
   if(digitalRead(SIGNAL_PIN)== HIGH)  {
    lcd.setCursor(0,1);
      lcd.print("   Somebody ");
    }
    else  {
      lcd.setCursor(0,1);
       lcd.print("    Nobody! ");
    }
    delay(1000);

    

    
}
