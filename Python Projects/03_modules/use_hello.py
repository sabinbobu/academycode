import builtins # redundant
# - este inclus by default
# - include functii precum open(), print,...

import hello
from hello import say_hello, N

# dorim sa se execute block ul de cod din if numai daca s-a apelat use_hello.py

print(__name__)

if __name__ == '__main__':  # daca s-a apelat python use_hello.py
	say_hello('Gigi')
	print(N)

	hello.say_hello("Gigi")
