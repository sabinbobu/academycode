import threading


class BankAccount:
    __slots__ = ('__owner', '__amount', '__mutex', '__cond')

    def __init__(self, owner, amount=0):
        self.__owner = owner
        self.__amount = amount
        self.__mutex = threading.RLock()
        self.__cond = threading.Condition(self.__mutex)

    def deposit(self, value):
        with self.__mutex:
            self.__amount += value  # ne-atomica
            self.__cond.notify_all()

    def withdraw(self, value):
        with self.__cond:
            while self.__amount < value:
                print('Prea putini bani in cont')
                self.__cond.wait()
            print('Acum e ok')
            self.__amount -= value

    @property
    def amount(self):
        with self.__mutex:
            return self.__amount

    def __str__(self):
        return '{}: {}'.format(self.__owner, self.amount)


def consume(account):
    for i in range(1000):
        account.withdraw(10)
        # print('{} Withdaw: {}'.format(threading.current_thread().native_id,
        #                              account))


def produce(account):
    for i in range(2000):
        account.deposit(5)
        # print('{} Deposit: {}'.format(threading.current_thread().native_id,
        #                              account))


if __name__ == '__main__':
    ion = BankAccount('Ion')

    producers = [
        threading.Thread(target=produce, args=(ion, )),
        threading.Thread(target=produce, args=(ion, )),
        threading.Thread(target=produce, args=(ion, )),
        threading.Thread(target=produce, args=(ion, )),
    ]

    consumers = [
        threading.Thread(target=consume, args=(ion, )),
        threading.Thread(target=consume, args=(ion, )),
        threading.Thread(target=consume, args=(ion, )),
        threading.Thread(target=consume, args=(ion, )),
    ]

    for p, c in zip(producers, consumers):
        p.start()
        c.start()

    print('Waiting workers...')
    for p, c in zip(producers, consumers):
        p.join()
        c.join()

    print(ion)
