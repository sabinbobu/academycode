import threading
import time


def say_hello():
    print('Hello from thread {} - {}'.format(
        threading.current_thread().name,
        threading.current_thread().native_id))
    time.sleep(1)
    print('Thread {} - {} is ready'.format(
        threading.current_thread().name,
        threading.current_thread().native_id))


def test_basic():
    t = threading.Thread(name='worker', target=say_hello)
    t.start()
    print('[MAIN - {}] Waiting for thread to finish...'.format(
        threading.current_thread().native_id))
    t.join(2)
    print('OK')


def do_work(evt, timeout):
    while not evt.is_set():
        print('Waiting event...')
        evt_set = evt.wait(timeout)
        print('Processing... ' if evt_set else 'Timed out!')


def test_event():
    ev = threading.Event()
    t = threading.Thread(name='worker', target=do_work, args=(ev, 5))
    t.start()

    time.sleep(6)
    ev.set()
    print('[MAIN] set the event')
    t.join(2)


def test_timer_thread():
    t1 = threading.Timer(2, say_hello)
    t2 = threading.Timer(1, say_hello)
    t1.setName('trigger1')
    t2.setName('trigger2')

    print('[MAIN - {}] Starting trigers'.format(
        threading.current_thread().native_id))
    t1.start()
    t2.start()

    time.sleep(10)  # doing stuff...
    print('[MAIN] Cancelling...')
    t1.cancel()
    t2.cancel()

    print('[MAIN] OK')



if __name__ == '__main__':
    # test_basic()
    # test_timer_thread()
    test_event()
