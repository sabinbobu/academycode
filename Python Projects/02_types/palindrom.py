# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 09:31:11 2021

@author: F86588D
"""

# capac
# cojoc

def palindrom1(s):
    return s == s[::-1]

def palindrom2(s):
    for i in range(0, len(s)//2):
        if s[i] == s[len(s) - i - 1]:
            return False
    return True

s = "capac"  
s_invers = []
for litera in s[::-1]:
    s_invers.append(litera)    

print(s)    
print(palindrom1(s))
print(palindrom2(s))
print(s_invers)