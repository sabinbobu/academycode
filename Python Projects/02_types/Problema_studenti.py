# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 14:50:56 2021
@author: F86588D

Se da un fisier students.txt cu structura:
Ion Ionescu; mate:10, 9, 10; romana: 9, 8, 7; sport: 10, 10
Maria Marinescu; mate: ; romana: 8, 10; istorie: 10
.
.
.

Catalog de studenti in fisier.

Sa se afiseze studentii cu cele mai mari 2 medii generale    
- Nume Compelt: Media pe materii= . Media generala=
"""

from operator import itemgetter

# =============================================================================
# Citire date din fisier
# =============================================================================

# def avg_materie(lista_note):
#     suma = 0
#     if len(lista_note) !=0:
#         for i in lista_note:
#             suma += i
#         return suma / len(lista_note)
#     else:
#         return 0

def avg_student(student):
    suma = 0
    for materie in student['materii']:
        if len(materie['note']) != 0:
            media = sum(materie['note']) / len(materie['note'])
        else:
            media = 0
        materie['medie'] = media
        suma += media
    media_generala = suma / len(student['materii'])
    student['generala'] = media_generala
       
def display(studenti):
    for student in studenti:
        print(student['nume'])
        for materie in student['materii']:
            print(materie['denumire'], end=':')
            print(materie['note'])
            print(materie.get('medie') or '')
        print("Media generala: ")
        print((student.get('generala')) or '')
        print()
    
def find_best(studenti):
    studenti.sort(key=itemgetter('generala'), reverse = True)
    mark = studenti[0]['generala']
    n = 0
    while studenti[n]['generala'] == mark:
        n += 1
    if n < len(studenti):
        mark = studenti[n]['generala']
        while studenti[n]['generala'] == mark:
            n += 1
    return studenti[:n]

def readFile():
    #declarare lista de studenti
    studenti = []
    with open('studenti.txt', 'rt', encoding='utf-8') as f:
        for line in f:
            line = line.strip() # eliminam white spaces
            lista = line.split(';')
            #print(info)
            # Dictionar pentru student
            student = {}
            student['nume'] = lista[0].strip()
            student['materii'] = list() # lista goala ~ []
            for n in lista[1:]: # parcurgem lista dupa nume complet
                n = n.strip()
                #print(n)
                #print()
                nume_materie = n.split(':',)[0] # din lista rez: =>ex:  mate
                note = n.split(':')[1] # din lista rez: => ex: 10, 9
                
                note = note.split(',')
                note = [nota.strip() for nota in note]
                note = [float(nota) for nota in note if nota != '']
                #print(note)
                materie = {'denumire': nume_materie, 'note': note} # dictionar
                student['materii'].append(materie) # adaugam dictionarul materie
            studenti.append(student)
    return studenti
            
        
 # ----- apel functii -----       
studenti = readFile() # readFile() va return o lista de studenti

print('==========================================')

for student in studenti:
   avg_student(student) 
   ##print(student)
   #print('-----------------------------------------------------------------')
   
display(studenti)

best = find_best(studenti)

print("========= BEST students: ================= ")
print()
display(best)
