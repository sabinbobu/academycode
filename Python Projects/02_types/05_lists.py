# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 09:50:36 2021

@author: F86588D
"""
import sys # chestii legate de sistem
# ne da si lista de arg de la command line

def test_relate_classic():
    names = ['Ana', 'Maria', 'Andrei']
    marks = [10, 8, 10]
    
    n = min(len(names), len(marks))
    for i in range(n):
        print('{}: {}'.format(names[i], marks[i]))

def test_relate_better():
    names = ['Ana', 'Maria', 'Andrei']
    marks = [10, 8, 10]
    ages = list(range(15, 20))
    
    for name, mark, age in zip(names, marks, ages): # zip ( fermuar ) creeaza aceasta pereche
        print('Name: {}: Mark: {}, Age: {}'.format(name, mark, age))

def test_more():
    names = ['Ana', 'Maria', 'Andrei']
    marks = [10, 8, 10]
    ages = list(range(15, 20))
    
    for i, (name, mark, age) in enumerate(zip(names, marks, ages)): # zip ( fermuar ) creeaza aceasta pereche
        print('{} - {}: Mark: {}, Age: {}'.format(i, name, mark, age))
        
    for i, value in enumerate(zip(names, marks, ages)): # zip ( fermuar ) creeaza aceasta pereche
        print('{} - {}: Mark: {}, Age: {}'.format(i, value[0], value[1], value[2]))
        
test_relate_classic()
test_relate_better()
test_more()

for name in sys.argv[1:]:# argv[0] da numele programului
    print('Hello {}'.format(name))
        