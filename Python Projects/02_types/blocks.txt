
// alocare memorie pointer
int *pi = (int*) malloc(sizeof(int));
//use pi...
if(pi != NULL )
	free(p); // otherwise we have a memory leak


// alocare memorie matrice

int **create(int l, int c )
{
  int **mat=(int**)malloc(l * sizeof(int*));
  if(mat == NULL)
    return NULL;

  for(int i=0;i<l;i++)
  {
  	mat[i]=(int*) malloc (c * sizeof(int));
  	if(mat[i] == NULL){
        for(int j=0; j<i; j++)
            free(mat[j]);
        free(mat);
        return NULL;
  	}

  	 for(int j=0;j<c;j++)
  	 	mat[i][j]=10;
	}
  return mat;
}


// alocare memorie matrice - varianta mai cache friendly 

int *create1d(int l, int c){
    int *mat = (int*) malloc(l*c*sizeof(int));
    for(int i=0;i<l;i++){
        for(int j=0;j<c;j++)
        {
            mat[i*c+j] = 10;
        }
    }

    return mat;
}

kdev@edocti
