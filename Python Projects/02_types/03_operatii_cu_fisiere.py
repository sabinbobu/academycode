# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:43:04 2021

@author: F86588D
"""
print("------------- Begin Var 1 -------------")

f = open('blocks.txt', 'rt', encoding='utf-8') # cp-1252

for line in f:
    print(line, end='')

f.close()

print(" ------------ End var 1 --------")

print("------------- Begin Var 2 -------------")
f = None
try:
    f = open('blocks.txt', 'rt', encoding='cp-1252') # cp-1252
    for line in f:
        print(line, end='')
except Exception as e:
    print('Error: ' + str(e))
finally:
    if f is not None:
        f.close()

print(" ------------ End var 2 --------")

print("------ Begin Var 3 ( Recomandat ) --------")

with open('blocks.txt', 'rt', encoding='utf-8') as f:
    line = f.readline()
    lines = f.readlines()
    print("Nr linii citite: ")
    print(len(lines))
    
    f.seek(len(line))
    print(line.upper(), end='')
    for line in f:
        print(line, end='')
    print(type(line))
        
print(" --------  End Var 3 --------------")

print(" -------- Deschidere in mod binar ------------")

# aici nu va alica un encoding la deschiderea fisierului
with open('blocks.txt', 'rb') as f:
    data = f.read() # citeste tot fisierul si il pune in data
    print(data.decode('utf-8'))
    print(type(data))
    print("Lungime fisier in Bytes")
    print(len(data))