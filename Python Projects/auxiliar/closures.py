# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 16:21:42 2021

@author: F86588D
"""

# Python program to illustrate
# closures
def outerFunction(text):
	text = text

	def innerFunction():
		print(text)

	# Note we are returning function
	# WITHOUT parenthesis
	return innerFunction

if __name__ == '__main__':
	myFunction = outerFunction('Hey!')
	myFunction()
