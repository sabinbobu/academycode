def increment(value, amount=1):
    return value + amount

#gresit, pentru ca nu putem seta un argument cu valoare implicita ca primul argument al liste, 
# le setam dupa primul arg 
#def f(amount = 1, value):
#    return value + amount

#  y este un argument default ( poate sa lipseasca la call - > va avea val 20 
#def f(value, x, y=20)
#    print('{}, {}, {}'.format(value, x, y))
    

# lista de argumente
def g(*args):
    for arg in args:
        print(arg) 

def h(*args, **kwargs):
    for a in args:
        print(a)
    print(kwargs)


#call function  -----------------
x = 100
y = 5

# trimitere prin copiere, dar pozitional 
print(increment(x, y))

# ne folosim de valoarea default a functiei ( y = 1 )
print(increment(x))

# putem pune param in orice ordine dorim cu conditia sa da numele param
print(increment(amount=100, value=x))

# amount = 1 ( valoarea default )
print(increment(value=x))

g(1, 2, 3, 4)
g(1, 2, 3)
params = [ 1, 2, 3, 4]

# va trata params ca fiind un singur argument
print("g(params):")
g(params)

print("g(*params):")
g(*params)



#f(10, 20 30)
d = {'value':10, 'y':20, 'x':30}
# putem pasa argumente catre functie ca si dictionar
#f(**d)
#f(**{'value':10, 'y':20, 'x':30})


h(1, 2, 3, **d)

print("h(1, 2, 3, x, y)")
h(1, 2, 3, x, y)

#f(y="Ana",x= "Gigi",value=100)
