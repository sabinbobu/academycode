def test_numeric_format():
    f = 1000.23
    print('{:6.3f}'.format(f))  # va folosi minim 6 caractere pentru afisarea nr
    numere = [10.23, 2.7, 304, 100.222, 10.1, -9.99]

    for n in numere:

        print('{:09.3f}'.format(n))
        print('{:>10.3f}'.format(n))  # aliniere la dr
        print('{:<10.3f}'.format(n))  # aliniere la stg
        print('{:^10.3f}'.format(n))  # aliniere la centru 
        print('{:+10.3f}'.format(n))  # pune semnul nr
        print('{:<+10.3f}'.format(n))  # pune semnul nr si aliniere la stg

    print('{:06d}'.format(100))  # pe min 6 cifre, decimal, fill cu 0 in fata


def test_string_format():
    names = ('Ana', 'Maria', 'Bobu Sabin Daniel')
    for name in names:
        print('{:10s}'.format(name))
        print('{:>10s}'.format(name))
        print('{:+>10s}'.format(name))
        # truncate & padding
        print('{:10.5}'.format(name))  # nr de carac max 10, cu trunchiere la 5
        print()

if __name__ == '__main__':
#    test_numeric_format()
    test_string_format()

# 1000.230
# 00010.230
#     10.230
# 10.230    
#   10.230  
#    +10.230
# 00002.700
#      2.700
# 2.700     
#   2.700   
#     +2.700
# 00304.000
#    304.000
# 304.000   
#  304.000  
#   +304.000
# 00100.222
#    100.222
# 100.222   
#  100.222  
#   +100.222
# 00010.100
#     10.100
# 10.100    
#   10.100  
#    +10.100
# -0009.990
#     -9.990
# -9.990    
#   -9.990  
#     -9.990
# 000100
