"""functie care ia ca si arg o  lista pe mai multe nivele ( 2d )
    si o transformam intr-o lista pe un singur nivel 
[10, 20, 1, [3, 4, [2, 1, [10], 34]], 10, 12, [13, 14]]

= > [10, 20, 1, 3, 4, 2, 1, 10, 34, 10, 12, 13, 14]

"""

def aplatizeaza1(lista):
    source = str(lista)
    dest = ''
    for i in source:
        if i != '[' and i != ']':
            dest += i
    
    dest = dest.split(',')
    return [int(r) for r in dest ]
     
   

def aplatizeaza2(lista):
    source = str(lista)
    dest = [i for i in source if i not in '[]']
    dest = ''.join(dest)    
    dest = dest.split(',')
    
    return [int(r) for r in dest ]
     

def aplatizeaza3(lista, dest):
    for i in lista:
        if isinstance(i, list):
            aplatizeaza3(i, dest)
        else:
            dest.append(i) # il adaugam in lista noua
       
       
def aplatizeaza4(lista):
    for e in lista:
        if isinstance(e, list):
            for i in aplatizeaza4(e):
                yield i
        else:
            yield e
            
lista = [10, 20, 1, [3, 4, [2, 1, [10], 34]], 10, 12, [13, 14]]
lista3 = [10, 20, 'Sabin', [3, 4, [2, 1, [10], 34]], 10.7, 12, [13, 14]]

print(aplatizeaza1(lista))

print(aplatizeaza2(lista))

dest = []
aplatizeaza3(lista3, dest)
print(dest)

dest4 = list(aplatizeaza4(lista))
print(dest)


