def forbidden():
    print("I cannot change this!")


def another():
    print('another')


def decorate(f):
    def wrapper():
        print('Something before')
        f()
        print('Something after')

    return wrapper


forbidden = decorate(forbidden)
forbidden()

another = decorate(another)
another()

