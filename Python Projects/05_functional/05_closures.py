# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 15:48:05 2021

@author: F86588D
"""
def say_hello():
    global name  # = 'Maria'
    name = 'Maria'
    city = 'Cluj'
    print('Hello {}'.format(name))


"""
1. a = 10 
2. intram in outer() - > a = 10
3. print(a=10)
4. are loc definirea func inner()
5. a se face 2
6. print(a = 2)
7. apel inner(2 + 2)
"""


def outer():
    global a
    print('Outer scope step 1 a = {}'.format(a))

    def inner(b):
        print('Inner scope a = {}'.format(a))
        return a + b
    a = 2
    print('Outer scope step 2 a = {}'.format(a))
    return inner(a + 2)


def priority_sort(l, important):
    found  = False
    print(found)
    def prio_selector(x):
        # In Python, these non-local variables can be accessed only within their scope and not outside their scope.
        # ii permitem variabilei found sa caute valoare in interiorul mediului extern funtiei in care a fost folosita
        nonlocal found
        if x in important:
            found  = True
            return (0, x)
        else:
            return (1, x)
    l.sort(key=prio_selector)
    print(l)
    print(found)


if __name__ == '__main__':
    a = 10
    name = 'Ana'
    say_hello()
    print(name)

    print('Outer() = {}'.format(outer()))
    print('a = {}'.format(a))
    
    priority_sort([1, 9, 2, 5, 6], [9,6])
