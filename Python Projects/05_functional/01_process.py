def process(names, operation):
    result = [] 
    for n in names:
        result.append(operation(n))
    return result


if __name__ == '__main__':
    names = ['aNa', 'marian', 'Ioana']
    print(process(names, str.capitalize))
    print(process(names, str.upper))
    print(process(names, len))
    print(process(names, str.lower))
