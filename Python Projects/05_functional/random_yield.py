import random 
import time

possible_values = [ 111, 222 , 333, 444, 555, 666, 777]

def infinite(values):
    while True:
        yield random.choice(values)
        
        
if __name__ == '__main__':
    gen = infinite(possible_values)  # function call infinite(values)
    for i in range(20):
        print(gen.__next__())
        print()
        time.sleep(0.5)
    
    
        
