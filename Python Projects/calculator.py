from decimal import *


def add(tokens):
    # list comprehension
    numere = [Decimal(x) for x in tokens if x != '']
    return sum(numere)


def sub(tokens):
    numere = [Decimal(x) for x in tokens if x != '']
    if len(numere) == 0:
        return 'Too few arguments'
    return numere[0] - sum(numere[1:])


def prod(tokens):
    numere = [Decimal(x) for x in tokens if x != '']
    res = 1
    for n in numere:
        res *= n
    return res


def div(tokens):
    numere = [Decimal(x) for x in tokens if x != '']
    if len(numere) == 0:
        return 'Too few arguments'
    impartitor = prod(numere[1:])
    if impartitor == 0:
        return 'Division by zero'
    return numere[0] / impartitor


def dblsum(tokens):
    return 2 * add(tokens)


def not_supported(tokens):
    return 'Not supported'


operations = {'add': add, 'sub': sub, 'prod': prod, 'div': div, 'dblsum': dblsum}

while True:
    expr = input('expr: ').lstrip()
    if expr.strip().lower() == 'end':
        break

    tokens = expr.split(' ')
    op = operations.get(tokens[0].lower()) or not_supported
    try:
        res = op(tokens[1:])  # sequence slicing
    except (ValueError, InvalidOperation) as e:
        res = 'Invalid operands'
#    except InvalidOperation as e:
#        res = 'Invalid operands'
    finally:
        print(res)

