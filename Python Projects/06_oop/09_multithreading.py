# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 15:53:25 2021

@author: F86588D
"""

import threading
import time


def say_hello():
    print('Hello from thread {} - {}'.format(threading.current_thread().name()), 
          threading.current_thread().native_id())
    time.sleep(1)
    print('Thread {} - {} is ready'.format(threading.current_thread().name()),
          threading.current_thread().native_id())
    
def test_basic():
    t = threading.Thread(name='worker', target=say_hello)
    t.start()
    print('[MAIN] Waiting for thread to finish...')
    t.join(2) # 2 sec de asteptare dupa thread
    print('Finished')

def do_work(evt, timeout):
    while not evt.is_set():
        print('Waiting event...')
        evt_set = evt.wait(timeout)
        print('Processing...' if evt_set else 'Time out')
        
        
    
def test_event():
    ev = threading.Event()
    t = threading.Thread(name='worker', target=do_work, args=(ev, 5))
    t.start()
    
    time.sleep(3)
    ev.set()
    print('[MAIN] set the event')
    t.join(2)
    

def test_timer_thread():
    t1 = threading.Timer(2, say_hello)
    t2 = threading.Timer(1, say_hello)
    t1.setName('Trigger 1')
    t1.setName('Trigger 2')
    
    print(' [MAIN} Starting triggers')
    t1.start()
    t2.start()
    
    time.sleep(1) # duing stuff...
    print('[MAIN] Cancelling...')
    t1.cancel()
    t2.cancel()
    
    print('OK')
    

if __name__ == '__main__':
    # test_basic()
    test_timer_thread()
    
