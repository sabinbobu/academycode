from datetime import date, datetime


class Person(object):
    __slots__ = ('__first_name', '__last_name', '__domain', '__birth_date')
    # cu slots nu vom mai putea adauga alte campuri(ATRIBUTE) in dictionar
    # slots activeaza un alt mechanism de gestionare a opiectelor, anume slot urile
    # se trece de la dictionar la tuple
    # asta inseamna ca doar atributele specificate in slots vor aparea in clasa
    def __init__(self, first_name, last_name, domain, birth_date):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__domain = domain
        self.__birth_date = birth_date

    @property
    def full_name(self):
        return '{} {}'.format(self.__first_name, self.__last_name)

    @full_name.setter
    def full_name(self, full_name):
        self.__first_name, self.__last_name = (x.capitalize() for x in full_name.split()[:2])

    @property
    def email(self):
        return '{}.{}@{}'.format(self.__first_name, self.__last_name, self.__domain).lower()

    @property
    def age(self):
        return date.today().year - self.birth_date.year

    # operator overloading
    def __gt__(self, other):
        return self.age > other.age

    def __lt__(self, other):
        return not self.__gt__(other)
        # return not self > other

    def __eq__(self, other):
        return self.full_name == other.full_name and self.age == other.age


ion = Person('Ion', 'Ionescu', 'gmail.com', date(year=1990, month=3, day=10))
vasile = Person('Vasile', 'Vasilescu', 'gmail.com', date(1989, 4, 10))

# nu mai merge, dupa ce am setat slots
# ion.cnp = '123456789'
# print(ion.cnp)

print(ion.full_name)

# Pentru a accesa un atribut setat ascuns:
print(ion._Person__domain)