class Student:
    __slots__ = '__first_name', '__last_name', '__materii'
    def __init__(self, first_name, last_name):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__materii = list()
        
    def add_materie(self, materie):
        self.__materii.append(materie)
        
    @property
    def full_name(self):
        return '{} {}'.format(self.__first_name, self.__last_name)
    
    def __str__(self):
        s = self.full_name + '\n'
        for i, m in enumerate(self.__materii, 1):
            s += '\t{} {}\n'.format(i, str(m))
        return s


class Materie:
    def __init__(self, denumire, note):
        self.denumire = denumire
        self.note = note  
    
    def __str__(self):
        return '{}: {}'.format(self.denumire, self.note)
    
    
def citire():
    students = []
    with open('studenti.txt', 'rt') as f:
        for line in f:
            line = line.strip()
            lista = line.split(';')
            
            student = Student(*lista[0].strip().split()[:2])
            for n in lista[1:]: #nu vreau sa merg de la 0
                n = n.strip()
                #print(n)
                nume_materie = n.split(':')[0] #ia doar primul elem
                note = n.split(':')[1] #ia notele
                note = note.split(',')
                note = [nota.strip() for nota in note]
                note = [float(nota) for nota in note if nota != '']
                #print(note)
                materie = Materie(nume_materie, note)
                student.add_materie(materie)
            students.append(student)
    return students
        
if __name__ == '__main__':
    students = citire() 
    for student in students:
        print(student)      
        
        
        
        