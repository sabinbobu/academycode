# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 09:23:21 2021

@author: F86588D
"""

class Vehicle:
    # variabilele cu __ in fata sunt ascunse
    def __init__(self, marca, model, cmc, age, vin):
        self.__marca = marca
        self.__model = model
        self.__cmc = cmc
        self.__age = age
        self.__vin = vin
        
    # suprascriere de constructor ( nu putem face overload ing, e permisa numai o implementare)
    # def __init__(self, marca, model, cmc):
    #     self.__marca = marca
    #     self.__model = model    
        
    def start(self):
        print(f'{self.marca} {self.model} starting...')
    
    def stop(self):
        print(f'{self.marca} {self.model} stopping...')
    
    #  pentru a ascunde metoda inc_age() in afara clasei punem 2 __
    #  daca ii punem __ si la final, va redeveni o functie obisnuita
    def __inc_age(self):
        self.__age += 1
    
    def __str__(self):
        return '{} {}'.format(self.__marca, self.__model)
    
    def __repr__(self):
        # return str(self) #self.__str__() - > ia un obiect si il transforma in str
        return '{} {} {}'.format(self.__marca, self.__model, self.__vin)
    

if __name__ == '__main__':
    
    kia = Vehicle('Kia', 'Niro', 1800, 2, '12312344123412')
    vw = Vehicle('VW', 'Golf', 1900, 5, '312431254312')
    
    # Chiar daca este privata (__), metoda poate fi accesata in ext cu:
    #kia.inc_age()
    kia._Vehicle__inc_age()
    # trebuie sa punem si _Vehicle in fata pentru ca __age este " privat "
    print(kia._Vehicle__age)
    print(kia)
    
    print(repr(kia))
    
    
    