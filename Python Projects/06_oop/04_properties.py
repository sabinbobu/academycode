from datetime import date, datetime


class Person(object):
    def __init__(self, first_name, last_name, domain, birth_date):
        self.first_name = first_name
        self.last_name = last_name
        self.domain = domain
        self.birth_date = birth_date

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @full_name.setter
    def full_name(self, full_name):
        self.first_name, self.last_name = (x.capitalize() for x in full_name.split()[:2])

    @property
    def email(self):
        return '{}.{}@{}'.format(self.first_name, self.last_name, self.domain).lower()

    @property
    def age(self):
        return date.today().year - self.birth_date.year


ion = Person('Ion', 'Ionescu', 'gmail.com', date(year=1990, month=3, day=10))
print(ion.full_name)
print(ion.email)
ion.full_name = 'ionicA IonEScu Georgescu'
print(ion.full_name)
print(ion.email)
print(ion.age)


now = date.today()
print(now)
now = datetime.now()
print(now)
