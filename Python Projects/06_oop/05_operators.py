from datetime import date, datetime


class Person(object):
    def __init__(self, first_name, last_name, domain, birth_date):
        self.first_name = first_name
        self.last_name = last_name
        self.domain = domain
        self.birth_date = birth_date

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    @full_name.setter
    def full_name(self, full_name):
        self.first_name, self.last_name = (x.capitalize() for x in full_name.split()[:2])

    @property
    def email(self):
        return '{}.{}@{}'.format(self.first_name, self.last_name, self.domain).lower()

    @property
    def age(self):
        return date.today().year - self.birth_date.year

    # operator overloading
    def __gt__(self, other):
        return self.age > other.age

    def __lt__(self, other):
        return not self.__gt__(other)
        # return not self > other

    def __eq__(self, other):
        return self.full_name == other.full_name and self.age == other.age


ion = Person('Ion', 'Ionescu', 'gmail.com', date(year=1990, month=3, day=10))
vasile = Person('Vasile', 'Vasilescu', 'gmail.com', date(1989, 4, 10))
print(ion > vasile)
print(ion < vasile)
print(ion == vasile)
ionica = ion
print(ionica == ion)

ionel = Person('Ion', 'Ionescu', 'yahoo.com', date(1990, 5, 25))
print(ion == ionel)
print(ion is ionel) # in java: ion.equals(ionel)

print(ion.__sizeof__())