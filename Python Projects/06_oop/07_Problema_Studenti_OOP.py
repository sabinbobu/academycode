
"""
Problema cu studenti
Afisare studentii cu cele mai mari 3 medii.
"""

class Student:
    __slots__ = ('__first_name', '__last_name', '__materii')
    def __init__(self, first_name, last_name):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__materii = list()
    
    def __str__(self):
        string = self.full_name + '\n'
        for i, m in enumerate(self.__materii, 1):
            string += '\t{} {} - {:.2f}\n'.format(i, str(m), m.average)
        
        string += '{:>8.2f}'.format(self.medie_generala)
        return string
    
    def add_materie(self, materie):
        self.__materii.append(materie)
    
    @property
    def full_name(self):
        return '{} {}'.format(self.__first_name, self.__last_name)
    
    @property 
    def medie_generala(self):
        suma = 0
        if len(self.__materii) == 0:
            return 0
        for materie in self.__materii:
            suma += materie.average
        return suma / len(self.__materii)
        
        # return sum( [ m.average for m in self.__materii ] ) / len(self.__materii) if len(self.__materii) > 0 else 0
        
    def __gt__(self, other):
        return self.medie_generala > other.medie_generala
        
    
    
class Materie:
    __slots__ = ('__denumire', '__note')
    def __init__(self, denumire, note):
        self.__denumire = denumire
        self.__note = note
    
    def __str__(self):
        return '{}: {}'.format(self.__denumire, self.__note)
    
    # functie care sa calculeze media notelor pentru fiecare materie
    # astfel vom avea media ca si proprietate a clasei care poate fi data mai departe
    @property 
    def average(self):
        suma = 0
        if len(self.__note) == 0:
            return 0
        for nota in self.__note:
            suma += nota
        return suma/len(self.__note)
    
        # return sum(self.__note) / len(self.__note) if len(self.__note) > 0 else 0
    
    def adauga_nota(self, nota):
        self.__note.append(nota)
    
    

def citire_din_fisier():
    students = []
    with open('studenti.txt', 'rt', encoding='utf-8') as f:
        for line in f:
            line = line.strip() # eliminam white spaces
            lista = line.split(';')
        
            student = Student(*lista[0].strip().split()[:2])
            # am pus * in fata ca sa stie ca va primi mai multe argumente
            # 2, in cazul asta (dupa spli())
            
            for n in lista[1:]: # parcurgem lista dupa nume complet
                n = n.strip()
                
                nume_materie = n.split(':',)[0] # din lista rez: =>ex:  mate
                note = n.split(':')[1] # din lista rez: => ex: 10, 9
                note = note.split(',')
                note = [nota.strip() for nota in note]
                note = [float(nota) for nota in note if nota != '']

                materie = Materie(nume_materie, note)
                student.add_materie(materie) # adaugam materie
            students.append(student)
    return students

# def find_best(studenti):
#     studenti.sort(reverse = True)
#     mark = studenti[0].medie_generala
    
#     n = 0
#     while studenti[n].medie_generala == mark:
#         n += 1
#     if n < len(studenti):
#         mark = studenti[n].medie_generala
#         while studenti[n].medie_generala == mark:
#             n += 1
#     return studenti[:n]

def find_best(studenti):
    studenti.sort(reverse = True)
    marks = [s.medie_generala for s in studenti]
    n = marks.count(marks[0])    
    
    if n < len(studenti):
        n += marks.count(marks[n])

    return studenti[:n]

if __name__ == '__main__':
    students = citire_din_fisier()
    
    # Aadaugare student nou
    
    student_nou = Student('Bobu', "Sabin")
    
    materii = [
        Materie('Fizica', [10, 10, 10]), 
        Materie('Engleza', [9, 9, 10]),
        Materie('Sport', [10, 10, 10])
        ]
    materii[0].adauga_nota(10)
    
    for materie in materii:
        student_nou.add_materie(materie)
        
    students.append(student_nou)
    
    for student in students:
        print(student)
        
    print()
    
    best = find_best(students)
    
    print(' ----------------- Best Students --------------- ')
    
    for student in best:
        print(student)
        
    print(' -------------------------------------------------  ')
        