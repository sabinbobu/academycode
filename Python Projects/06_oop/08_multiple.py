

class Base1(object):
    def __init__(self, x):
        print('Base1')
        self.x = x

    def __del__(self):
        print('~Base1')

    def f(self):
        print('B1.f()')


class Base2:
    def __init__(self, y):
        print('Base2')
        self.y = y

    def __del__(self):
        print('~Base2')

    def f(self):
        print('B2.f()')


class Derived(Base1, Base2):
    def __init__(self, x, y, z):
        Base1.__init__(self, x)
        Base2.__init__(self, y)
        self.z = z

    def __str__(self):
        return '{}, {}, {}'.format(self.x, self.y, self.z)


#    def __del__(self):
#        Base1.__del__(self)
#        Base2.__del__(self)
#        print('~Derived')


if __name__ == '__main__':
    d = Derived(10, 20, 30)
    print(d)
    d.f()
