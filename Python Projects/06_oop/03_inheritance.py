class Person(object):
    def __init__(self, first_name, last_name, cnp, address):
        self.first_name = first_name
        self.last_name = last_name
        self.cnp = cnp
        self.address = address
        self.__initialize_person()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __initialize_person(self):
        pass

    def __del__(self):
        print('Destroying {} {}...'.format(self.first_name, self.last_name))
        
    def merge(self):
        print('{} {} merge...'.format(self.first_name, self.last_name))


class Student(Person):
    def __init__(self, first_name, last_name, cnp, address, faculty):
        Person.__init__(self, first_name, last_name, cnp, address)
        self.faculty = faculty

    def __str__(self):
        # return f'{self.first_name} {self.last_name}, student la {self.faculty}'
        return '{}, student la {}'.format(Person.__str__(self), self.faculty)


class Pensionar(Person):
    def __init__(self, first_name, last_name, cnp, address, pensie):
        Person.__init__(self, first_name, last_name, cnp, address)
        self.pensie = pensie

    def __str__(self):
        # return f'{self.first_name} {self.last_name}, pensionar'
        return '{}, pensionar'.format(Person.__str__(self))


def test():
    ion = Student('Ion', 'Popescu', '236453645', 'Arad', 'ASE')
    print(ion)
    print('La revedere!')


def test_out(person):
    person.first_name = person.first_name.upper()
    print(person)
    print('Bye bye!')
    print()


if __name__ == '__main__':
    print(' ---------- test() ---------------')
    test()
    print()
    marius = Pensionar('Marius', 'Marinescu', '2342342', 'Arad', 3000)
    
    print('--------- test_out(marius) -------------')
    test_out(marius)

    people = [
        Person('Ion', 'Ionescu', '1212', 'Timisoara'),
        Student('Pop', 'Popescu', '3253245', 'Cluj', 'AC'),
        Pensionar('Gigi', 'Ionescu', '76547645', 'Bucuresti', 3000)
    ]

    # Polimorfism
    for person in people:
        print(person.__str__())
        print(person.first_name)

    people[2].merge()
