class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
    #  printarea propriu-zisa ( __str__ printeaza de obicei adresa obiectului)
    #  face o suprascriere
    def __str__(self):
        return '{} {}, age = {}'.format(self.first_name, self.last_name,
                                        self.age)
    
    def speak(self):
        print('{} {} vorbeste...'.format(self.first_name, self.last_name))


if __name__ == '__main__':
    ion = Person("Ion", "Ionescu", 20)
    gigi = Person("Gigi", "Becali", 50)
    
    print(ion)
    print(gigi)
    ion.speak()
    Person.speak(gigi)

    print(ion.first_name)
    ion.first_name = 'Ionel'
    print(ion)
    
    ion.cnp = '1980220330551'
    print(ion.cnp)
    
    #print(gigi.cnp)
    
    # Fiecare clasa in Python are un membru special __dict__ care retine un dictionar care are ca si chei membrii clasei

    print(ion.__dict__)
    ion.__dict__['first_name'] = 'Ion'
    print(ion)
    
    print(gigi.__dict__)
    
    